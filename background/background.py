#!/usr/bin/env python
"""
An example consumer that uses a greenlet pool to accept incoming market
messages. This example offers a high degree of concurrency.
"""
import zlib
# This can be replaced with the built-in json module, if desired.
import simplejson

import zmq.green as zmq
import redis;
import logging;
import reader;
import statistics_processor
import history_statistics_processor
import time;

import gevent
from gevent.pool import Pool
from gevent import monkey; gevent.monkey.patch_all()


# The maximum number of greenlet workers in the greenlet pool. This is not one
# per processor, a decent machine can support hundreds or thousands of greenlets.
# I recommend setting this to the maximum number of connections your database
# backend can accept, if you must open one connection per save op.
MAX_NUM_POOL_WORKERS = 250

def main():
    """
    The main flow of the application.
    """
    context = zmq.Context()
    subscriber = context.socket(zmq.SUB)

    # Connect to the first publicly available relay.
    subscriber.connect('tcp://relay-us-west-1.eve-emdr.com:8050')
    # Disable filtering.
    subscriber.setsockopt(zmq.SUBSCRIBE, "")

    # We use a greenlet pool to cap the number of workers at a reasonable level.
    greenlet_pool = Pool(size=MAX_NUM_POOL_WORKERS)

    print("Consumer daemon started, waiting for jobs...")
    print("Worker pool size: %d" % greenlet_pool.size)

    r = redis.StrictRedis(host='localhost', port=6379, db=0)

    r.set('reader_processed', 0)
    r.set('reader_processed_history', 0)
    r.set('statistics_processed', 0)
    r.set('history_statistics_processed', 0)
    FORMAT = '[%(levelname)s] %(asctime)-15s %(message)s'
    logging.basicConfig(format=FORMAT,level=logging.DEBUG)
    l = logging.getLogger('background')
    for i in range(0, 5):
        l.info('Start reader '+str(i))
        reader.Reader(i).start();

    for i in range(0, 5):
        l.info('Start stats processor '+str(i))
        statistics_processor.StatisticsProcessor(i).start();

    for i in range(0, 5):
        l.info('Start history stats processor '+str(i))
        history_statistics_processor.HistoryStatisticsProcessor(i).start();

    while True:
        # Since subscriber.recv() blocks when no messages are available,
        # this loop stays under control. If something is available and the
        # greenlet pool has greenlets available for use, work gets done.
        if (greenlet_pool.full()):
            l.info('Pool full, waiting to spawn new')
        greenlet_pool.spawn(worker, subscriber.recv(), r)
        read = r.get('reader_processed')
        history = r.get('reader_processed_history')
        stats = r.get('statistics_processed')
        history_stats = r.get('history_statistics_processed')
        l.info('Working... '+str(greenlet_pool.free_count())+' greenlets available, '+str(read)+' orders, '+str(history)+' history, '+str(stats) + ' statistics made, '+str(history_stats) + ' history statistics made')


def worker(job_json, r):
    """
    For every incoming message, this worker function is called. Be extremely
    careful not to do anything CPU-intensive here, or you will see blocking.
    Sockets are async under gevent, so those are fair game.
    """
    # Receive raw market JSON strings.
    try:
        market_json = zlib.decompress(job_json)
        # Un-serialize the JSON data to a Python dict.
        #market_data = simplejson.loads(market_json)
        r.rpush('market_data', market_json)
        len = r.llen('market_data');
        if (len > 2000):
            r.ltrim('market_data', -1001, -1)
    except:
        pass



if __name__ == '__main__':
    main()

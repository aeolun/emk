__author__ = 'bart'

import threading;
import redis;
import logging;
import simplejson;
import time;
from sqlalchemy import create_engine, Column, Integer, String, Numeric, DateTime, Date, Boolean;
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import datetime
from dateutil import parser
from decimal import *
import warnings
from sqlalchemy import exc as sa_exc
from scipy.stats import *
from scipy.stats.mstats import mquantiles
from numpy import *
import time
import math

Base = declarative_base()
class Statistial_Info(Base):
    __tablename__ = 'statistical_info'

    region_id = Column(Integer, primary_key=True)
    type_id = Column(Integer, primary_key=True)
    buy_available = Column(Integer)
    buy_count = Column(Integer)
    buy_min = Column(Numeric(20,2))
    buy_max = Column(Numeric(20,2))
    buy_average = Column(Numeric(20,2))
    buy_mean = Column(Numeric(20,2))
    buy_percentile90 = Column(Numeric(20,2))
    buy_percentile10 = Column(Numeric(20,2))
    sell_available = Column(Integer)
    sell_count = Column(Integer)
    sell_min = Column(Numeric(20,2))
    sell_max = Column(Numeric(20,2))
    sell_average = Column(Numeric(20,2))
    sell_mean = Column(Numeric(20,2))
    sell_percentile90 = Column(Numeric(20,2))
    sell_percentile10 = Column(Numeric(20,2))
    updated = Column(DateTime)

class Order(Base):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True)
    region_id = Column(Integer)
    type_id = Column(Integer)
    solar_system_id = Column(Integer)
    station_id = Column(Integer)
    price = Column(Numeric(20,2))
    volume_remaining = Column(Integer)
    volume_entered = Column(Integer)
    volume_minimum = Column(Integer)
    is_bid = Column(Integer)
    issued = Column(DateTime)
    duration = Column(Integer)
    bullshit = Column(Boolean)

class StatisticsProcessor(threading.Thread):
    def __init__(self, nr, *args, **kwargs):
        super(StatisticsProcessor, self).__init__(*args, **kwargs)
        self.nr = nr

    def run(self):
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore")
            r = redis.StrictRedis(host='localhost', port=6379, db=0)
            engine = create_engine('mysql://root:uithoorn@localhost/emktwo', echo=False)
            Session = sessionmaker(bind=engine)
            session = Session()

            getcontext().prec = 3

            FORMAT = '[%(levelname)s] %(asctime)-15s %(message)s'
            logging.basicConfig(format=FORMAT,level=logging.DEBUG)
            l = logging.getLogger('backgroundstats'+str(self.nr))
            while True:
                try:
                    data = r.blpop('statistics')
                    if data is not None:
                        #l.info('order processing');
                        #print data
                        orders = simplejson.loads(data[1])
                        stats = {}
                        for type in ['buy', 'sell']:
                            if len(orders[type]['prices']) > 0:
                                if len(orders[type]['prices']) > 5:


                                    stats[type+'_quantiles'] = mquantiles(orders[type]['prices'])
                                    lowest = stats[type+'_quantiles'][0]
                                    highest = stats[type+'_quantiles'][2]
                                    iqd = highest - lowest
                                    delete = []
                                    for index, id in enumerate(orders[type]['ids']):
                                        bullshit = False
                                        if type == 'sell':
                                            if orders[type]['prices'][index] > highest + iqd*2.5:
                                                #print "marking "+str(index)+" where "+str(orders[type]['prices'][index]) + " > " + str(highest + iqd*2.5)
                                                bullshit = True
                                        if type == 'buy':
                                            if orders[type]['prices'][index] < lowest - iqd*2.5:
                                                #print "marking "+str(index)+" where "+str(orders[type]['prices'][index]) + " < " + str(lowest - iqd*2.5)
                                                bullshit = True
                                        if bullshit == True:
                                            #mark as bullshit
                                            row = session.query(Order).filter(Order.id==id).first()
                                            row.bullshit = True;

                                            delete.append(index)
                                    delete = sorted(delete, reverse=True)
                                    for index in delete:
                                        #print "deleting index "+str(index)
                                        del orders[type]['prices'][index]
                                        del orders[type]['ids'][index]
                                        del orders[type]['amounts'][index]
                                    try:
                                        session.commit();
                                    except:
                                        session.rollback();



                                stats[type+'_mean'] = tmean(orders[type]['prices'])
                                stats[type+'_min'] = min(orders[type]['prices'])
                                stats[type+'_max'] = max(orders[type]['prices'])
                                stats[type+'_available'] = sum(orders[type]['amounts'])
                                stats[type+'_count'] = len(orders[type]['prices'])
                                stats[type+'_average'] = average(orders[type]['prices'], weights=orders[type]['amounts'])


                                list = zip(orders[type]['prices'], orders[type]['amounts'])
                                list.sort()

                                maximum = float(stats[type+'_available'])/10.0
                                maximum = int(ceil(maximum))

                                cur = 0
                                cum_price = 0
                                #print "min10"
                                for i in range(0, len(list)):
                                    price = list[i][0]
                                    amount = list[i][1]
                                    if (cur+amount <= maximum):
                                        pass
                                    else:
                                        newamount = maximum-cur
                                        #print 'trimming from '+str(amount)+' to '+str(newamount)
                                        amount = newamount
                                    cur+=amount
                                    cum_price+=amount*price
                                    #print str(i)+' ('+str(cur)+'/'+str(maximum)+'): '+str(amount)+'*'+str(price)
                                    if (cur >= maximum): break
                                min10 = cum_price / maximum

                                list.reverse()

                                cur = 0
                                cum_price = 0
                                #print "max10"
                                for i in range(0, len(list)):
                                    price = list[i][0]
                                    amount = list[i][1]
                                    if (cur+amount <= maximum):
                                        pass
                                    else:
                                        newamount = maximum-cur
                                        #print 'trimming from '+str(amount)+' to '+str(newamount)
                                        amount = newamount
                                    cur+=amount
                                    cum_price+=amount*price
                                    #print str(i)+' ('+str(cur)+'/'+str(maximum)+'): '+str(amount)+'*'+str(price)
                                    if (cur >= maximum): break
                                max10 = cum_price / maximum

                                perc = 95
                                stats[type+'_percentile90'] = max10
                                stats[type+'_percentile10'] = min10
                            else:
                                stats[type+'_mean'] = None
                                stats[type+'_min'] = None
                                stats[type+'_max'] = None
                                stats[type+'_available'] = None
                                stats[type+'_count'] = None
                                stats[type+'_average'] = None
                                stats[type+'_percentile90'] = None
                                stats[type+'_percentile10'] = None

                        temp = session.query(Statistial_Info).filter(Statistial_Info.type_id==orders['type_id']).filter(Statistial_Info.region_id==orders['region_id']).first()
                        if (temp):
                            row = temp
                        else:
                            row = Statistial_Info()
                            session.add(row)
                        row.type_id = orders['type_id']
                        row.region_id = orders['region_id']
                        row.buy_available = stats['buy_available']
                        row.buy_min = stats['buy_min']
                        row.buy_max = stats['buy_max']
                        row.buy_mean = stats['buy_mean']
                        row.buy_count = stats['buy_count']
                        row.buy_average = stats['buy_average']
                        row.buy_percentile90 = stats['buy_percentile90']
                        row.buy_percentile10 = stats['buy_percentile10']
                        row.sell_available = stats['sell_available']
                        row.sell_min = stats['sell_min']
                        row.sell_max = stats['sell_max']
                        row.sell_mean = stats['sell_mean']
                        row.sell_count = stats['sell_count']
                        row.sell_average = stats['sell_average']
                        row.sell_percentile90 = stats['sell_percentile90']
                        row.sell_percentile10 = stats['sell_percentile10']
                        row.updated = time.strftime('%Y-%m-%d %H:%M:%S')

                        try:
                            session.commit();
                        except:
                            l.exception('Need to rollback')
                            session.rollback();

                        time.sleep(0.1);
                        r.incr('statistics_processed')
                except Exception:
                    l.exception("Exception in thread")


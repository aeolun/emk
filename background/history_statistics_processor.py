__author__ = 'bart'

import threading;
import redis;
import logging;
import simplejson;
import time;
from sqlalchemy import create_engine, Column, Integer, String, Numeric, DateTime, Date, Float;
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import datetime
from dateutil import parser
from decimal import *
import warnings
from sqlalchemy import exc as sa_exc
from scipy.stats import *
from numpy import *
import time
import math

Base = declarative_base()
class Historical_Summary(Base):
    __tablename__ = 'historical_summary'

    region_id = Column(Integer, primary_key=True)
    type_id = Column(Integer, primary_key=True)
    price_60d = Column(Float)
    price_20d = Column(Float)
    price_5d = Column(Float)
    high_60d = Column(Float)
    high_20d = Column(Float)
    high_5d = Column(Float)
    low_60d = Column(Float)
    low_20d = Column(Float)
    low_5d = Column(Float)
    bought_60d = Column(Float)
    bought_20d = Column(Float)
    bought_5d = Column(Float)
    sold_60d = Column(Float)
    sold_20d = Column(Float)
    sold_5d = Column(Float)
    orders_60d = Column(Float)
    orders_20d = Column(Float)
    orders_5d = Column(Float)
    transactions_60d = Column(Float)
    transactions_20d = Column(Float)
    transactions_5d = Column(Float)
    updated = Column(DateTime)

class HistoryStatisticsProcessor(threading.Thread):
    def __init__(self, nr, *args, **kwargs):
        super(HistoryStatisticsProcessor, self).__init__(*args, **kwargs)
        self.nr = nr

    def run(self):
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore")
            r = redis.StrictRedis(host='localhost', port=6379, db=0)
            engine = create_engine('mysql://root:uithoorn@localhost/emktwo', echo=False)
            Session = sessionmaker(bind=engine)
            session = Session()

            getcontext().prec = 3

            FORMAT = '[%(levelname)s] %(asctime)-15s %(message)s'
            logging.basicConfig(format=FORMAT,level=logging.DEBUG)
            l = logging.getLogger('backgroundhistorystats'+str(self.nr))
            while True:
                try:
                    data = r.blpop('history_statistics')
                    if data is not None:
                        #l.info('order processing');
                        #print data
                        history = simplejson.loads(data[1])
                        stats = {}
                        rollingstats = {
                            'price': 0,
                            'high': 0,
                            'low': 0,
                            'bought': 0,
                            'sold': 0,
                            'orders': 0,
                            'transactions': 0
                        }
                        for day in range(0, 60):
                            currentindex = len(history) - day - 1
                            row = history[currentindex]

                            rollingstats['price'] += row['average']
                            rollingstats['high'] += row['high']
                            rollingstats['low'] += row['low']
                            rollingstats['orders'] += row['orders']
                            rollingstats['transactions'] += row['quantity']

                            if row['high'] > 0 and row['low']:
                                if (row['high']-row['low'] <> 0):
                                    sell_proportion = (row['average']-row['low'])/(row['high']-row['low'])
                                else:
                                    sell_proportion = 0.5
                                buy_proportion = 1 - sell_proportion
                                rollingstats['bought'] += row['quantity']*buy_proportion
                                rollingstats['sold'] += row['quantity']*sell_proportion
                            else:
                                rollingstats['bought'] += 0
                                rollingstats['sold'] += 0

                            days_passed = day + 1

                            if (days_passed == 5 or days_passed == 20 or days_passed == 60):
                                stats['price_'+str(days_passed)+'d'] = rollingstats['price']/days_passed
                                stats['bought_'+str(days_passed)+'d'] = rollingstats['bought']/days_passed
                                stats['sold_'+str(days_passed)+'d'] = rollingstats['sold']/days_passed
                                stats['orders_'+str(days_passed)+'d'] = rollingstats['orders']/days_passed
                                stats['transactions_'+str(days_passed)+'d'] = rollingstats['transactions']/days_passed
                                stats['high_'+str(days_passed)+'d'] = rollingstats['high']/days_passed
                                stats['low_'+str(days_passed)+'d'] = rollingstats['low']/days_passed

                        temp = session.query(Historical_Summary).filter(Historical_Summary.type_id==history[0]['type_id']).filter(Historical_Summary.region_id==history[0]['region_id']).first()
                        if (temp):
                            row = temp
                        else:
                            row = Historical_Summary()
                            session.add(row)
                        row.type_id = history[0]['type_id']
                        row.region_id = history[0]['region_id']

                        row.price_60d = stats['price_60d']
                        row.price_20d = stats['price_20d']
                        row.price_5d = stats['price_5d']
                        row.high_60d = stats['high_60d']
                        row.high_20d = stats['high_20d']
                        row.high_5d = stats['high_5d']
                        row.low_60d = stats['low_60d']
                        row.low_20d = stats['low_20d']
                        row.low_5d = stats['low_5d']
                        row.bought_60d = stats['bought_60d']
                        row.bought_20d = stats['bought_20d']
                        row.bought_5d = stats['bought_5d']
                        row.sold_60d = stats['sold_60d']
                        row.sold_20d = stats['sold_20d']
                        row.sold_5d = stats['sold_5d']
                        row.orders_60d = stats['orders_60d']
                        row.orders_20d = stats['orders_20d']
                        row.orders_5d = stats['orders_5d']
                        row.transactions_60d = stats['transactions_60d']
                        row.transactions_20d = stats['transactions_20d']
                        row.transactions_5d = stats['transactions_5d']

                        row.updated = time.strftime('%Y-%m-%d %H:%M:%S')

                        try:
                            session.commit();
                        except:
                            l.exception('Need to rollback')
                            session.rollback();

                        time.sleep(0.1);
                        r.incr('history_statistics_processed')
                except Exception:
                    l.exception("Exception in thread")


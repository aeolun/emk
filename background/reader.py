__author__ = 'bart'

import threading;
import redis;
import logging;
import simplejson;
import time;
from sqlalchemy import create_engine, Column, Integer, String, Boolean, Numeric, DateTime, Date;
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import datetime
from dateutil import parser
from decimal import *
import warnings
import time
from sqlalchemy import exc as sa_exc
import pytz

Base = declarative_base()
class Order(Base):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True)
    region_id = Column(Integer)
    type_id = Column(Integer)
    solar_system_id = Column(Integer)
    station_id = Column(Integer)
    price = Column(Numeric(20,2))
    volume_remaining = Column(Integer)
    volume_entered = Column(Integer)
    volume_minimum = Column(Integer)
    is_bid = Column(Integer)
    issued = Column(DateTime)
    duration = Column(Integer)
    bullshit = Column(Boolean)

class History(Base):
    __tablename__ = 'history'

    date = Column(Date, primary_key=True)
    region_id = Column(Integer, primary_key=True)
    type_id = Column(Integer, primary_key=True)
    low = Column(Numeric(20,2))
    high = Column(Numeric(20,2))
    average = Column(Numeric(20,2))
    orders = Column(Integer)
    quantity = Column(Integer)


class Reader(threading.Thread):
    def __init__(self, nr, *args, **kwargs):
        super(Reader, self).__init__(*args, **kwargs)
        self.nr = nr

    def run(self):
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore")
            r = redis.StrictRedis(host='localhost', port=6379, db=0)
            engine = create_engine('mysql://root:uithoorn@localhost/emktwo', echo=False)
            Session = sessionmaker(bind=engine)
            session = Session()

            getcontext().prec = 3

            FORMAT = '[%(levelname)s] %(asctime)-15s %(message)s'
            logging.basicConfig(format=FORMAT,level=logging.DEBUG)
            l = logging.getLogger('backgroundreader'+str(self.nr))
            while True:

                try:
                    data = r.blpop('market_data')
                    if data is not None:
                        market_data = simplejson.loads(data[1])
                        identify_string = str(market_data['rowsets'][0]['regionID'])+'/'+str(market_data['rowsets'][0]['typeID'])+'/'+market_data['resultType']
                        last = r.get(identify_string)

                        generated = parser.parse(market_data['rowsets'][0]['generatedAt'])
                        if (last != None):
                            alright = float(last)
                            l.info(identify_string+': '+str(datetime.datetime.fromtimestamp(alright))+' < '+str(market_data['rowsets'][0]['generatedAt']))
                            if (alright > time.mktime(generated.timetuple())):
                                l.info("Skipped because newer information exists")
                                continue;

                        r.set(identify_string, time.mktime(generated.timetuple()))
                        if (market_data['resultType'] == 'orders'):
                            for rowset in market_data['rowsets']:

                                temp = session.query(Order).filter(Order.type_id==rowset['typeID']).filter(Order.region_id==rowset['regionID']).all()
                                existing = {}
                                for row in temp:
                                    existing[row.id] = row
                                new = {
                                    'region_id': rowset['regionID'],
                                    'type_id': rowset['typeID'],
                                    'sell': {
                                        'prices': [],
                                        'amounts': [],
                                        'ids': []
                                    },
                                    'buy': {
                                        'prices': [],
                                        'amounts': [],
                                        'ids': []
                                    }
                                }
                                do_commit = True
                                for row in rowset['rows']:
                                    if (row[3] in existing):
                                        ord = existing[row[3]]
                                        existing[row[3]] = None
                                    else:
                                        ord = Order()
                                        session.add(ord)

                                    ord.id = row[3]
                                    ord.region_id = rowset['regionID']
                                    ord.type_id = rowset['typeID']
                                    ord.solar_system_id = row[10]
                                    ord.station_id = row[9]
                                    ord.price = round(row[0],2)
                                    ord.volume_remaining = row[1]
                                    ord.volume_entered = row[4]
                                    ord.volume_minimum = row[5]
                                    ord.is_bid = row[6]
                                    date = parser.parse(row[7])
                                    ord.issued = date.strftime("%Y-%m-%d")
                                    ord.duration = row[8]

                                    #determine whether this order is bullshit
                                    bullshit = False
                                    if ord.volume_minimum > ord.volume_entered / 2 and ord.volume_entered > 3:
                                        bullshit = True
                                    if ord.price == 0:
                                        do_commit = False
                                        break;
                                    if ord.region_id == 0 or ord.type_id == 0 or ord.station_id == 0 or ord.solar_system_id == 0:
                                        do_commit = False
                                        break;
                                    if ord.price < 0.5:
                                        bullshit = True
                                    if date == None:
                                        do_commit = False
                                        break;

                                    ord.bullshit = bullshit

                                    if ord.bullshit == False:
                                        type = 'buy' if ord.is_bid else 'sell'
                                        new[type]['ids'].append(ord.id)
                                        new[type]['prices'].append(ord.price)
                                        new[type]['amounts'].append(ord.volume_remaining)
                                if do_commit == True:
                                    count = 0
                                    for still in existing:
                                        if existing[still] is not None:
                                            session.delete(existing[still]);
                                            count+=1
                                    #l.info('deleted '+str(count)+' rows')
                                    try:
                                        session.commit()
                                        r.rpush("statistics", simplejson.dumps(new))
                                    except:
                                        session.rollback();
                                    #l.info("updated/inserted "+str(len(rowset['rows']))+" rows")
                                else:
                                    l.info("Orders were incorrect, skipping this commit")
                            r.incr('reader_processed')
                        elif (market_data['resultType'] == 'history'):
                            for rowset in market_data['rowsets']:
                                if len(rowset['rows']) > 0:
                                    ins = []
                                    for row in rowset['rows']:
                                        hist = {}
                                        date = parser.parse(row[0])
                                        hist['date'] = date.strftime("%Y-%m-%d")
                                        hist['region_id'] = rowset['regionID']
                                        hist['type_id'] = rowset['typeID']
                                        hist['low'] = round(row[3],2)
                                        hist['high'] = round(row[4],2)
                                        hist['average'] = round(row[5],2)
                                        hist['orders'] = row[1]
                                        hist['quantity'] = row[2]

                                        ins.append(hist)

                                    if len(ins) > 60:
                                        r.rpush("history_statistics", simplejson.dumps(ins))
                                    try:
                                        inserter = History.__table__.insert().prefix_with('IGNORE')
                                        session.execute(inserter, ins)
                                        #l.info('inserted '+str(len(rowset['rows']))+" history rows")
                                        #session.commit()
                                    except Exception as e:
                                        l.exception(e)
                                        session.rollback();
                                    #l.info("updated/inserted "+str(len(rowset['rows']))+" rows")
                            r.incr('reader_processed_history')

                        #l.info("Thread "+str(self.nr)+" parsed "+market_data['resultType']+" - "+str(len(market_data['rowsets'][0]['rows'])))
                        time.sleep(0.1);
                except Exception:
                    l.exception("Exception in thread")


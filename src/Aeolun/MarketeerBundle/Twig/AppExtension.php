<?php

namespace Aeolun\MarketeerBundle\Twig;

/**
 * Created by PhpStorm.
 * User: Bart
 * Date: 12/28/2016
 * Time: 11:10 AM
 */
class AppExtension extends \Twig_Extension
{
    public function getName() {
        return "exploooooosion";
    }

    public function getFunctions() {
        return [
            new \Twig_SimpleFunction('region_list', array($this, 'regionList'))
        ];
    }

    public function regionList() {
        return "regionList";
    }
}
<?php

namespace Aeolun\MarketeerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Aeolun\MarketeerBundle\Entity\ItemType;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TypeController extends BaseController
{
    public function indexAction($id)
    {
	    $itemRepo = $this->getDoctrine()->getRepository('Aeolun\MarketeerBundle\Entity\ItemType');
		$res = $itemRepo->find($id);

	    if ($this->getUser() == null) die('Please log in first, sorry still broken otherwise.');

        return $this->render('MarketeerBundle:Type:index.html.twig', [
			'type' => $res
        ]);
    }
}

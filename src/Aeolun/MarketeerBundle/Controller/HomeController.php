<?php

namespace Aeolun\MarketeerBundle\Controller;

use Aeolun\MarketeerBundle\Entity\Banner;
use Aeolun\MarketeerBundle\Entity\News;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class HomeController extends BaseController
{
    public function indexAction()
    {
	    $newsRepo = $this->getDoctrine()->getRepository(News::class);
        $bannerRepo = $this->getDoctrine()->getRepository(Banner::class);

        $cache = new FileSystemAdapter();

        $rss = file_get_contents('https://forums.eveonline.com/default.aspx?g=rsstopic&pg=Topics&f=253');
        $xml = simplexml_load_string($rss);
        $forum = [];
        foreach($xml->channel->item as $item) {
            $forum[(string)$item->title] = (string)$item->link;
        }
        $forum = array_slice($forum, 0, 10)

        return $this->render('MarketeerBundle:Home:index.html.twig', [
	        'news' => $newsRepo->findBy([], ['postedAt'=>'desc']),
            'banners' => $bannerRepo->findAll(),
            'forum' => $forum
        ]);
    }

	public function regionSelectAction() {
		$regionRepo = $this->getDoctrine()->getRepository('Aeolun\MarketeerBundle\Entity\MapRegion');

		if ($this->getRequest()->isMethod('post')) {
			$regions = $this->getRequest()->get('region');
			$primaryRegion = $this->getRequest()->get('primaryRegion');

			$this->getUser()->clearRegions();
			foreach($regions as $regionId) {
				$this->getUser()->addRegion($regionRepo->find($regionId));
			}
			$this->getUser()->setPrimaryRegion($regionRepo->find($primaryRegion));

			$this->getDoctrine()->getManager()->flush();

			return $this->redirect($this->generateUrl('home'));
		} else {
			return $this->render('MarketeerBundle:Home:regions.html.twig', [
				'regions' => $regionRepo->findBy([], ['regionName'=>'asc'])
			]);
		}
	}
}

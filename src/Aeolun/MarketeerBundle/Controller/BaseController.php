<?php
/**
 * User: Bart
 * Date: 8-12-13
 * Time: 14:25
 * 
 */

namespace Aeolun\MarketeerBundle\Controller;


use Aeolun\MarketeerBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller {

	/**
	 * @return User
	 */
	function getUser() {
		return parent::getUser();
	}
} 
<?php

namespace Aeolun\MarketeerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Aeolun\MarketeerBundle\Entity\ItemType;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SearchController extends BaseController
{
    public function indexAction()
    {
	    $m = $this->getDoctrine()->getManager();
	    $itemRepo = $m->getRepository('Aeolun\MarketeerBundle\Entity\ItemType');

	    $search = $this->getRequest()->query->get('q');

	    $results = $itemRepo->createQueryBuilder('o')->where('o.typeName LIKE :search')->setParameter('search', '%'.$search.'%')->getQuery()->getResult();

        return $this->render('MarketeerBundle:Search:index.html.twig', [
	        'results' => $results
        ]);
    }
}

<?php
/**
 * User: Bart
 * Date: 27-11-13
 * Time: 22:07
 * 
 */

namespace Aeolun\MarketeerBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SDECommand extends Command {

	var $start;
	var $progress;
	var $replacements = array(
		array('/InnoDB/', "INNODB"),
		//general replace of ugly things
		array('/(`[^` ]*?)([a-z])([A-Z])([^` ]*?`)/e', "'$1$2_'.strtolower('$3').'$4'"),
		array('/(`[^` ]*?)([a-z])([A-Z])([^` ]*?`)/e', "'$1$2_'.strtolower('$3').'$4'"),
		array('/(`[^` ]*?)([a-z])([A-Z])([^` ]*?`)/e', "'$1$2_'.strtolower('$3').'$4'"),
		array('/(`[^` ]*?)([a-z])([A-Z])([^` ]*?`)/e', "'$1$2_'.strtolower('$3').'$4'"),
		array('/(`[^` ]*?)([a-z])([A-Z])([^` ]*?`)/e', "'$1$2_'.strtolower('$3').'$4'"),
		array('/(`[^` ]*?)([a-z])([A-Z])([^` ]*?`)/e', "'$1$2_'.strtolower('$3').'$4'"),
		array('/(`[^` ]*?)([a-z])([A-Z])([^` ]*?`)/e', "'$1$2_'.strtolower('$3').'$4'"),
		array('/(`[^` ]*?)([a-z])([A-Z])([^` ]*?`)/e', "'$1$2_'.strtolower('$3').'$4'"),
		array('/iD/', "id"),
		array('/_i_d/', "_id"),
		array('/_n_p_c/', "_npc"),

		//specific replaces
		array('/agt_/', ""),
		array('/chr_/', "character_"),
		array('/crp_/', "corporation_"),
		array('/dgm_/', "dogma_"),
		array('/inv_/', "item_"),
		array('/ram_/', "industrial_"),
		array('/sta_/', "station_"),
		array('/trn_/', "translation_"),
		array('/crt_/', "certificate_"),

		array('/nPCCorp/', "npc_corp"),
		array('/corporation_npc_/', "npc_"),
		array('/nPCDivi/', "npc_divi"),
		array('/certificate_certificates/', "certificates"),
		array('/translation_translation_/', "translation_"),
		array('/translation_translations/', "translations"),
		array('/corporation_npc_divisions/', "npc_corporation_divisions"),
		array('/item_items/', "items"),

		array('/INNODB/', "InnoDB")
	);

	protected function configure()
	{
		$this
			->setName('sde:convert')
			->setDescription('Convert a MySQL sde file to have clean field and table names')
			->addArgument(
				'file',
				InputArgument::REQUIRED,
				'What file do you want to convert'
			)->addArgument(
				'outfile',
				InputArgument::REQUIRED,
				'Where do you want to output the converted file'
			)
		;
	}

	private function _progress(OutputInterface $output) {
		$output->writeln("[".round(microtime(true)-$this->start,3)."] ".$this->progress.'/'.count($this->replacements));
	}

	public function execute(InputInterface $input, OutputInterface $output) {
		$this->start = microtime(true);
		$this->progress = 0;
		$contents = file_get_contents($input->getArgument('file'));

		foreach($this->replacements as $replacement) {
			$this->progress++;
			$this->_progress($output);
			$contents = preg_replace($replacement[0], $replacement[1], $contents);
		}

		file_put_contents($input->getArgument('outfile'), $contents);
	}
} 
<?php
/**
 * User: Bart
 * Date: 27-11-13
 * Time: 22:07
 * 
 */

namespace Aeolun\MarketeerBundle\Command;

use Aeolun\MarketeerBundle\Entity\MapDenormalize;
use Aeolun\MarketeerBundle\Entity\MapSolarSystemDistance;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DijkstraCommand extends ContainerAwareCommand {

	protected function configure()
	{
		$this
			->setName('sde:dijkstra')
			->setDescription('Calculate Jump Distances')
		;
	}

	public function execute(InputInterface $input, OutputInterface $output) {
		/**
		 * @var Registry $doctrine
		 */
		$doctrine = $this->getContainer()->get('doctrine');

		$mapRepo = $doctrine->getManager()->getRepository('\Aeolun\MarketeerBundle\Entity\MapSolarSystemJumps');

		$safe = true;
		if ($safe) {
			$qb = $mapRepo->createQueryBuilder('m');
			$nodes = $qb->innerJoin('m.fromSolarSystem', 'fss')->
				innerJoin('m.toSolarSystem', 'tss')->
				where($qb->expr()->gte('fss.security', 0.5))->
				andWhere($qb->expr()->gte('tss.security', 0.5))->
				getQuery()->getResult();
		} else {
			$nodes = $mapRepo->findAll();
		}

		$maxOptions = count($nodes)*count($nodes);
		$routes = array();
		$unvisited = array();
		foreach($nodes as $node) {
			$routes[$node->getFromSolarSystem()->getSolarSystemId()][$node->getToSolarSystem()->getSolarSystemId()] = 1;
			$unvisited[$node->getFromSolarSystem()->getSolarSystemId()] = PHP_INT_MAX;
		}

		$from = null;
		$to = null;

		foreach($routes as $from=>$targets) {
			$output->writeln('calculating routes from '.$from.($safe?' in safe mode':' in unsafe mode'));
			$current = $from;

			$visited = array();
			$distance = array();
			$unvisited = array();
			$waypoints = array();
			foreach($nodes as $node) {
				$unvisited[$node->getFromSolarSystem()->getSolarSystemId()] = PHP_INT_MAX;
			}
			$stop = false;
			$start = microtime(true);
			$sorttime = 0;
			$it = 0;
			while($stop == false) {
				if ($current == $to) {
					$output->writeln('distance is '.$distance[$to]);
					$output->writeln('took '.(microtime(true)-$start).'s'.', '.$sorttime.' to sort');
					$stop = true;
				}
				$it++;
				$output->writeln($it.' of '.count($routes));
				//$output->writeln('current node '.$current.', routes to: '.implode(', ',array_keys($routes[$current])));
				//if (isset($routes[$current])) {
					foreach($routes[$current] as $toId => $length) {
						if(!in_array($toId, $visited)) {

							$newDistance = (isset($distance[$current])?$distance[$current]:0)+$length;
							$newWaypoints = (isset($waypoints[$current])?array_merge($waypoints[$current], array($toId)):array($toId));
							if (!isset($distance[$toId])) {
								$node = new MapSolarSystemDistance();
								$node->setSolarSystemId($from);
								$node->setToSolarSystemId($toId);
								$node->setIsSafe($safe);
								$node->setDistance($newDistance);
								$doctrine->getManager()->persist($node);
							}
							$distance[$toId] = $newDistance;
							$waypoints[$toId] = $newWaypoints;
							$output->writeln('waypoints: '.implode(', ', $waypoints[$toId]).'; distance '.$newDistance);
							$unvisited[$toId] = (isset($distance[$current])?$distance[$current]:0)+$length;
							//$output->writeln('distance to '.$toId.' is '.$length.' from start '.$distance[$toId]);


						}
					}
				//}
				unset($unvisited[$current]);
				if (count($unvisited) == 0) {
					arsort($distance);
					$key = array_keys($distance);
					$val = array_values($distance);
					$output->writeln('system '.$key[0].' is furthest away at '.$val[0].' jumps');
					$output->writeln('took '.(microtime(true)-$start).'s'.', '.$sorttime.' to sort');
					$stop = true;
				} else {
					$visited[] = $current;
					$s = microtime(true);
					asort($unvisited);
					$sorttime += (microtime(true)-$s);
					$current = array_keys($unvisited);
					$current = $current[0];
					$output->writeln('new current is '.$current);
				}
			}
			$doctrine->getManager()->flush();
		}

	}
} 
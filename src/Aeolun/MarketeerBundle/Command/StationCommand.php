<?php
/**
 * User: Bart
 * Date: 27-11-13
 * Time: 22:07
 * 
 */

namespace Aeolun\MarketeerBundle\Command;

use Aeolun\MarketeerBundle\Entity\MapDenormalize;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StationCommand extends ContainerAwareCommand {

	protected function configure()
	{
		$this
			->setName('sde:import')
			->setDescription('Import data from EVE api')
		;
	}

	public function execute(InputInterface $input, OutputInterface $output) {
		/**
		 * @var Registry $doctrine
		 */
		$doctrine = $this->getContainer()->get('doctrine');

		$mapdRepo = $doctrine->getManager()->getRepository('\Aeolun\MarketeerBundle\Entity\MapDenormalize');
		$stations = file_get_contents('https://api.eveonline.com/eve/ConquerableStationList.xml.aspx');

		$data = simplexml_load_string($stations);
		foreach($data->result->rowset->row as $row) {
			$attr = $row->attributes();

			$mapd = $mapdRepo->find($attr['stationID']);
			if ($mapd == null) {
				$mapd = new MapDenormalize();
			}
			$mapd->setItemId($attr['stationID']);
			$mapd->setItemName($attr['stationName']);
			$mapd->setSolarSystemId($attr['solarSystemID']);
			$mapd->setTypeId($attr['stationTypeID']);

			$doctrine->getManager()->persist($mapd);

			$output->writeln($attr['stationID'].': '.$attr['stationName']);
		}
		$doctrine->getManager()->flush();
	}
} 
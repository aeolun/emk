<?php
/**
 * User: Bart
 * Date: 14-12-13
 * Time: 15:27
 * 
 */

namespace Aeolun\MarketeerBundle\Doctrine;


use Aeolun\MarketeerBundle\DateTimeId;
use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class DateTimeIdType extends DateTimeType
{
	public function convertToPHPValue($value, AbstractPlatform $platform)
	{
		$dateTime = parent::convertToPHPValue($value, $platform);

		if ( ! $dateTime) {
			return $dateTime;
		}

		return new DateTimeId('@' . $dateTime->format('U'));
	}

	public function getName()
	{
		return 'datetimeid';
	}
}
<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * DogmaAttributeTypes
 *
 * @ORM\Table(name="dogma_attribute_types")
 * @ORM\Entity
 */
class DogmaAttributeTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="attribute_id", type="smallint", nullable=false)
     * @ORM\Id

     */
    private $attributeId;

    /**
     * @var string
     *
     * @ORM\Column(name="attribute_name", type="string", length=100, nullable=true)
     */
    private $attributeName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;

    /**
     * @var float
     *
     * @ORM\Column(name="default_value", type="float", nullable=true)
     */
    private $defaultValue;

    /**
     * @var integer
     *
     * @ORM\Column(name="published", type="integer", nullable=true)
     */
    private $published;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=100, nullable=true)
     */
    private $displayName;

    /**
     * @var integer
     *
     * @ORM\Column(name="unit_id", type="integer", nullable=true)
     */
    private $unitId;

    /**
     * @var integer
     *
     * @ORM\Column(name="stackable", type="integer", nullable=true)
     */
    private $stackable;

    /**
     * @var integer
     *
     * @ORM\Column(name="high_is_good", type="integer", nullable=true)
     */
    private $highIsGood;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     */
    private $categoryId;


}

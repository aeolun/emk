<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * PlanetSchematics
 *
 * @ORM\Table(name="planet_schematics")
 * @ORM\Entity
 */
class PlanetSchematics
{
    /**
     * @var integer
     *
     * @ORM\Column(name="schematic_id", type="smallint", nullable=false)
     * @ORM\Id

     */
    private $schematicId;

    /**
     * @var string
     *
     * @ORM\Column(name="schematic_name", type="string", length=510, nullable=true)
     */
    private $schematicName;

    /**
     * @var integer
     *
     * @ORM\Column(name="cycle_time", type="integer", nullable=true)
     */
    private $cycleTime;


}

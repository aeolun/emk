<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemFlags
 *
 * @ORM\Table(name="item_flags")
 * @ORM\Entity
 */
class ItemFlags
{
    /**
     * @var integer
     *
     * @ORM\Column(name="flag_id", type="smallint", nullable=false)
     * @ORM\Id

     */
    private $flagId;

    /**
     * @var string
     *
     * @ORM\Column(name="flag_name", type="string", length=200, nullable=true)
     */
    private $flagName;

    /**
     * @var string
     *
     * @ORM\Column(name="flag_text", type="string", length=100, nullable=true)
     */
    private $flagText;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=true)
     */
    private $orderId;


}

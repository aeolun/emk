<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * StationStationTypes
 *
 * @ORM\Table(name="station_station_types")
 * @ORM\Entity
 */
class StationStationTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="station_type_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $stationTypeId;

    /**
     * @var float
     *
     * @ORM\Column(name="dock_entry_x", type="float", nullable=true)
     */
    private $dockEntryX;

    /**
     * @var float
     *
     * @ORM\Column(name="dock_entry_y", type="float", nullable=true)
     */
    private $dockEntryY;

    /**
     * @var float
     *
     * @ORM\Column(name="dock_entry_z", type="float", nullable=true)
     */
    private $dockEntryZ;

    /**
     * @var float
     *
     * @ORM\Column(name="dock_orientation_x", type="float", nullable=true)
     */
    private $dockOrientationX;

    /**
     * @var float
     *
     * @ORM\Column(name="dock_orientation_y", type="float", nullable=true)
     */
    private $dockOrientationY;

    /**
     * @var float
     *
     * @ORM\Column(name="dock_orientation_z", type="float", nullable=true)
     */
    private $dockOrientationZ;

    /**
     * @var integer
     *
     * @ORM\Column(name="operation_id", type="integer", nullable=true)
     */
    private $operationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="office_slots", type="integer", nullable=true)
     */
    private $officeSlots;

    /**
     * @var float
     *
     * @ORM\Column(name="reprocessing_efficiency", type="float", nullable=true)
     */
    private $reprocessingEfficiency;

    /**
     * @var integer
     *
     * @ORM\Column(name="conquerable", type="integer", nullable=true)
     */
    private $conquerable;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapConstellationJumps
 *
 * @ORM\Table(name="map_constellation_jumps", indexes={@Index(name="map_constellation_jumps_IX_from_region", columns={"from_region_id"})})
 * @ORM\Entity
 */
class MapConstellationJumps
{
    /**
     * @var integer
     *
     * @ORM\Column(name="from_constellation_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $fromConstellationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="to_constellation_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $toConstellationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="from_region_id", type="integer", nullable=true)
     */
    private $fromRegionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="to_region_id", type="integer", nullable=true)
     */
    private $toRegionId;


}

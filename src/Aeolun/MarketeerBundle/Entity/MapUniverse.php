<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapUniverse
 *
 * @ORM\Table(name="map_universe")
 * @ORM\Entity
 */
class MapUniverse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="universe_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $universeId;

    /**
     * @var string
     *
     * @ORM\Column(name="universe_name", type="string", length=100, nullable=true)
     */
    private $universeName;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float", nullable=true)
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float", nullable=true)
     */
    private $y;

    /**
     * @var float
     *
     * @ORM\Column(name="z", type="float", nullable=true)
     */
    private $z;

    /**
     * @var float
     *
     * @ORM\Column(name="x_min", type="float", nullable=true)
     */
    private $xMin;

    /**
     * @var float
     *
     * @ORM\Column(name="x_max", type="float", nullable=true)
     */
    private $xMax;

    /**
     * @var float
     *
     * @ORM\Column(name="y_min", type="float", nullable=true)
     */
    private $yMin;

    /**
     * @var float
     *
     * @ORM\Column(name="y_max", type="float", nullable=true)
     */
    private $yMax;

    /**
     * @var float
     *
     * @ORM\Column(name="z_min", type="float", nullable=true)
     */
    private $zMin;

    /**
     * @var float
     *
     * @ORM\Column(name="z_max", type="float", nullable=true)
     */
    private $zMax;

    /**
     * @var float
     *
     * @ORM\Column(name="radius", type="float", nullable=true)
     */
    private $radius;


}

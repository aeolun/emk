<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;
use Doctrine\ORM\Mapping\UniqueConstraint as UniqueConstraint;

/**
 * Rules
 *
 * @ORM\Table(name="rules", uniqueConstraints={@UniqueConstraint(name="rules_rule_unique",columns={"rule"})})
 * @ORM\Entity
 */
class Rules
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rule", type="string", length=200, nullable=false)
     */
    private $rule;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     */
    private $description;


}

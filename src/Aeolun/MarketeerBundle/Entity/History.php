<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * Agents
 *
 * @ORM\Table(name="history")
 * @ORM\Entity
 */
class History
{
    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="datetimeid", nullable=false)
     * @ORM\Id
     */
    private $date;

	/**
	 * @var MapRegion $region
	 *
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="MapRegion", inversedBy="history")
	 * @ORM\JoinColumn(name="region_id", referencedColumnName="region_id", onDelete="CASCADE")
	 */
	private $region;

	/**
	 * @var ItemType $type
	 *
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="ItemType", inversedBy="history")
	 * @ORM\JoinColumn(name="type_id", referencedColumnName="type_id", onDelete="CASCADE")
	 */
	private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="low", type="decimal", scale=2, precision=20, nullable=true)
     */
    private $low;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="high", type="decimal", scale=2, precision=20, nullable=true)
	 */
	private $high;

	/**
	 * @var float
	 *
	 * @ORM\Column(name="average", type="decimal", scale=2, precision=20, nullable=true)
	 */
	private $average;

    /**
     * @var integer
     *
     * @ORM\Column(name="orders", type="integer", nullable=true)
     */
    private $orders;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="bigint", nullable=true)
     */
    private $quantity;

	/**
	 * @param float $average
	 */
	public function setAverage($average)
	{
		$this->average = $average;
	}

	/**
	 * @return float
	 */
	public function getAverage()
	{
		return $this->average;
	}

	/**
	 * @param \DateTime $date
	 */
	public function setDate($date)
	{
		$this->date = $date;
	}

	/**
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->date;
	}

	/**
	 * @param float $high
	 */
	public function setHigh($high)
	{
		$this->high = $high;
	}

	/**
	 * @return float
	 */
	public function getHigh()
	{
		return $this->high;
	}

	/**
	 * @param float $low
	 */
	public function setLow($low)
	{
		$this->low = $low;
	}

	/**
	 * @return float
	 */
	public function getLow()
	{
		return $this->low;
	}

	/**
	 * @param int $orders
	 */
	public function setOrders($orders)
	{
		$this->orders = $orders;
	}

	/**
	 * @return int
	 */
	public function getOrders()
	{
		return $this->orders;
	}

	/**
	 * @param int $quantity
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	}

	/**
	 * @return int
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * @param \Aeolun\MarketeerBundle\Entity\MapRegion $region
	 */
	public function setRegion($region)
	{
		$this->region = $region;
	}

	/**
	 * @return \Aeolun\MarketeerBundle\Entity\MapRegion
	 */
	public function getRegion()
	{
		return $this->region;
	}

	/**
	 * @param \Aeolun\MarketeerBundle\Entity\ItemType $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return \Aeolun\MarketeerBundle\Entity\ItemType
	 */
	public function getType()
	{
		return $this->type;
	}




}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * IndustrialActivities
 *
 * @ORM\Table(name="industrial_activities")
 * @ORM\Entity
 */
class IndustrialActivities
{
    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="integer", nullable=false)
     * @ORM\Id
     */
    private $activityId;

    /**
     * @var string
     *
     * @ORM\Column(name="activity_name", type="string", length=200, nullable=true)
     */
    private $activityName;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_no", type="string", length=5, nullable=true)
     */
    private $iconNo;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="published", type="integer", nullable=true)
     */
    private $published;


}

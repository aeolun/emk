<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemMetaTypes
 *
 * @ORM\Table(name="item_meta_types")
 * @ORM\Entity
 */
class ItemMetaTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_type_id", type="integer", nullable=true)
     */
    private $parentTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="meta_group_id", type="smallint", nullable=true)
     */
    private $metaGroupId;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * CertificateRecommendations
 *
 * @ORM\Table(name="certificate_recommendations", indexes={@Index(name="certificate_recommendations_IX_ship_type", columns={"ship_type_id"}), @Index(name="certificate_recommendations_IX_certificate", columns={"certificate_id"})})
 * @ORM\Entity
 */
class CertificateRecommendations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="recommendation_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $recommendationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="ship_type_id", type="integer", nullable=true)
     */
    private $shipTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="certificate_id", type="integer", nullable=true)
     */
    private $certificateId;

    /**
     * @var integer
     *
     * @ORM\Column(name="recommendation_level", type="integer", nullable=false)
     */
    private $recommendationLevel;


}

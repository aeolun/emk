<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * PlanetSchematicsPinMap
 *
 * @ORM\Table(name="planet_schematics_pin_map")
 * @ORM\Entity
 */
class PlanetSchematicsPinMap
{
    /**
     * @var integer
     *
     * @ORM\Column(name="schematic_id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $schematicId;

    /**
     * @var integer
     *
     * @ORM\Column(name="pin_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $pinTypeId;


}

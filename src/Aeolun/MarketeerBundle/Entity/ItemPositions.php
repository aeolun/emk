<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemPositions
 *
 * @ORM\Table(name="item_positions")
 * @ORM\Entity
 */
class ItemPositions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="bigint", nullable=false)
     * @ORM\Id

     */
    private $itemId;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float", nullable=false)
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float", nullable=false)
     */
    private $y;

    /**
     * @var float
     *
     * @ORM\Column(name="z", type="float", nullable=false)
     */
    private $z;

    /**
     * @var float
     *
     * @ORM\Column(name="yaw", type="float", nullable=true)
     */
    private $yaw;

    /**
     * @var float
     *
     * @ORM\Column(name="pitch", type="float", nullable=true)
     */
    private $pitch;

    /**
     * @var float
     *
     * @ORM\Column(name="roll", type="float", nullable=true)
     */
    private $roll;


}

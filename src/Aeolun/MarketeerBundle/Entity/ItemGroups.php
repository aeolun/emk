<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemGroups
 *
 * @ORM\Table(name="item_groups", indexes={@Index(name="item_groups_IX_category", columns={"category_id"})})
 * @ORM\Entity
 */
class ItemGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $groupId;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     */
    private $categoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="group_name", type="string", length=200, nullable=true)
     */
    private $groupName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=6000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;

    /**
     * @var integer
     *
     * @ORM\Column(name="use_base_price", type="integer", nullable=true)
     */
    private $useBasePrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="allow_manufacture", type="integer", nullable=true)
     */
    private $allowManufacture;

    /**
     * @var integer
     *
     * @ORM\Column(name="allow_recycler", type="integer", nullable=true)
     */
    private $allowRecycler;

    /**
     * @var integer
     *
     * @ORM\Column(name="anchored", type="integer", nullable=true)
     */
    private $anchored;

    /**
     * @var integer
     *
     * @ORM\Column(name="anchorable", type="integer", nullable=true)
     */
    private $anchorable;

    /**
     * @var integer
     *
     * @ORM\Column(name="fittable_non_singleton", type="integer", nullable=true)
     */
    private $fittableNonSingleton;

    /**
     * @var integer
     *
     * @ORM\Column(name="published", type="integer", nullable=true)
     */
    private $published;


}

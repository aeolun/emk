<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemBlueprintTypes
 *
 * @ORM\Table(name="item_blueprint_types")
 * @ORM\Entity
 */
class ItemBlueprintTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="blueprint_type_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $blueprintTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_blueprint_type_id", type="integer", nullable=true)
     */
    private $parentBlueprintTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="product_type_id", type="integer", nullable=true)
     */
    private $productTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="production_time", type="integer", nullable=true)
     */
    private $productionTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="tech_level", type="smallint", nullable=true)
     */
    private $techLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="research_productivity_time", type="integer", nullable=true)
     */
    private $researchProductivityTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="research_material_time", type="integer", nullable=true)
     */
    private $researchMaterialTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="research_copy_time", type="integer", nullable=true)
     */
    private $researchCopyTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="research_tech_time", type="integer", nullable=true)
     */
    private $researchTechTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="productivity_modifier", type="integer", nullable=true)
     */
    private $productivityModifier;

    /**
     * @var integer
     *
     * @ORM\Column(name="material_modifier", type="smallint", nullable=true)
     */
    private $materialModifier;

    /**
     * @var integer
     *
     * @ORM\Column(name="waste_factor", type="smallint", nullable=true)
     */
    private $wasteFactor;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_production_limit", type="integer", nullable=true)
     */
    private $maxProductionLimit;


}

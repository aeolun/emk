<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * EveIcons
 *
 * @ORM\Table(name="eve_icons")
 * @ORM\Entity
 */
class EveIcons
{
    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $iconId;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_file", type="string", length=500, nullable=false)
     */
    private $iconFile;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;


}

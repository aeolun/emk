<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * StationOperations
 *
 * @ORM\Table(name="station_operations")
 * @ORM\Entity
 */
class StationOperations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="operation_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $operationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="integer", nullable=true)
     */
    private $activityId;

    /**
     * @var string
     *
     * @ORM\Column(name="operation_name", type="string", length=200, nullable=true)
     */
    private $operationName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="fringe", type="integer", nullable=true)
     */
    private $fringe;

    /**
     * @var integer
     *
     * @ORM\Column(name="corridor", type="integer", nullable=true)
     */
    private $corridor;

    /**
     * @var integer
     *
     * @ORM\Column(name="hub", type="integer", nullable=true)
     */
    private $hub;

    /**
     * @var integer
     *
     * @ORM\Column(name="border", type="integer", nullable=true)
     */
    private $border;

    /**
     * @var integer
     *
     * @ORM\Column(name="ratio", type="integer", nullable=true)
     */
    private $ratio;

    /**
     * @var integer
     *
     * @ORM\Column(name="caldari_station_type_id", type="integer", nullable=true)
     */
    private $caldariStationTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="minmatar_station_type_id", type="integer", nullable=true)
     */
    private $minmatarStationTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="amarr_station_type_id", type="integer", nullable=true)
     */
    private $amarrStationTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="gallente_station_type_id", type="integer", nullable=true)
     */
    private $gallenteStationTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="jove_station_type_id", type="integer", nullable=true)
     */
    private $joveStationTypeId;


}

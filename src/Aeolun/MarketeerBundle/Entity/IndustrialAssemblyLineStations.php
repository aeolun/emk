<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * IndustrialAssemblyLineStations
 *
 * @ORM\Table(name="industrial_assembly_line_stations", indexes={@Index(name="industrial_assembly_line_stations_IX_region", columns={"region_id"}), @Index(name="industrial_assembly_line_stations_IX_owner", columns={"owner_id"})})
 * @ORM\Entity
 */
class IndustrialAssemblyLineStations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="station_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $stationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="assembly_line_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $assemblyLineTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="station_type_id", type="integer", nullable=true)
     */
    private $stationTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="owner_id", type="integer", nullable=true)
     */
    private $ownerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="solar_system_id", type="integer", nullable=true)
     */
    private $solarSystemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="region_id", type="integer", nullable=true)
     */
    private $regionId;


}

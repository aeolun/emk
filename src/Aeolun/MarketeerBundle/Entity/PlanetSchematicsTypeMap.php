<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * PlanetSchematicsTypeMap
 *
 * @ORM\Table(name="planet_schematics_type_map")
 * @ORM\Entity
 */
class PlanetSchematicsTypeMap
{
    /**
     * @var integer
     *
     * @ORM\Column(name="schematic_id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $schematicId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="smallint", nullable=true)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_input", type="integer", nullable=true)
     */
    private $isInput;


}

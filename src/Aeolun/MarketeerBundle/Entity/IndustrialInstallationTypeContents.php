<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * IndustrialInstallationTypeContents
 *
 * @ORM\Table(name="industrial_installation_type_contents")
 * @ORM\Entity
 */
class IndustrialInstallationTypeContents
{
    /**
     * @var integer
     *
     * @ORM\Column(name="installation_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $installationTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="assembly_line_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $assemblyLineTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * TranslationLanguages
 *
 * @ORM\Table(name="translation_languages")
 * @ORM\Entity
 */
class TranslationLanguages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="numeric_language_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $numericLanguageId;

    /**
     * @var string
     *
     * @ORM\Column(name="language_id", type="string", length=50, nullable=true)
     */
    private $languageId;

    /**
     * @var string
     *
     * @ORM\Column(name="language_name", type="string", length=400, nullable=true)
     */
    private $languageName;


}

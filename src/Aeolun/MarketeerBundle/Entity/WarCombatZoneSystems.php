<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * WarCombatZoneSystems
 *
 * @ORM\Table(name="war_combat_zone_systems")
 * @ORM\Entity
 */
class WarCombatZoneSystems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="solar_system_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $solarSystemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="combat_zone_id", type="integer", nullable=true)
     */
    private $combatZoneId;


}

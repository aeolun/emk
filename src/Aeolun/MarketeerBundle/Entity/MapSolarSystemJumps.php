<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapSolarSystemJumps
 *
 * @ORM\Table(name="map_solar_system_jumps", indexes={@Index(name="map_solar_system_jumps_IX_from_region", columns={"from_region_id"}), @Index(name="map_solar_system_jumps_IX_from_constellation", columns={"from_constellation_id"})})
 * @ORM\Entity
 */
class MapSolarSystemJumps
{
	/**
	 * @var MapSolarSystems $fromSolarSystem
	 *
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="MapSolarSystems", inversedBy="jumpsFrom")
	 * @ORM\JoinColumn(name="from_solar_system_id", referencedColumnName="solar_system_id", onDelete="CASCADE")
	 */
	private $fromSolarSystem;

	/**
	 * @var MapSolarSystems $toSolarSystem
	 *
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="MapSolarSystems", inversedBy="jumpsTo")
	 * @ORM\JoinColumn(name="to_solar_system_id", referencedColumnName="solar_system_id", onDelete="CASCADE")
	 */
    private $toSolarSystem;

    /**
     * @var integer
     *
     * @ORM\Column(name="from_region_id", type="integer", nullable=true)
     */
    private $fromRegionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="from_constellation_id", type="integer", nullable=true)
     */
    private $fromConstellationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="to_constellation_id", type="integer", nullable=true)
     */
    private $toConstellationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="to_region_id", type="integer", nullable=true)
     */
    private $toRegionId;

	/**
	 * @param int $fromConstellationId
	 */
	public function setFromConstellationId($fromConstellationId)
	{
		$this->fromConstellationId = $fromConstellationId;
	}

	/**
	 * @return int
	 */
	public function getFromConstellationId()
	{
		return $this->fromConstellationId;
	}

	/**
	 * @param int $fromRegionId
	 */
	public function setFromRegionId($fromRegionId)
	{
		$this->fromRegionId = $fromRegionId;
	}

	/**
	 * @return int
	 */
	public function getFromRegionId()
	{
		return $this->fromRegionId;
	}

	/**
	 * @param \Aeolun\MarketeerBundle\Entity\MapSolarSystems $fromSolarSystem
	 */
	public function setFromSolarSystem($fromSolarSystem)
	{
		$this->fromSolarSystem = $fromSolarSystem;
	}

	/**
	 * @return \Aeolun\MarketeerBundle\Entity\MapSolarSystems
	 */
	public function getFromSolarSystem()
	{
		return $this->fromSolarSystem;
	}

	/**
	 * @param int $toConstellationId
	 */
	public function setToConstellationId($toConstellationId)
	{
		$this->toConstellationId = $toConstellationId;
	}

	/**
	 * @return int
	 */
	public function getToConstellationId()
	{
		return $this->toConstellationId;
	}

	/**
	 * @param int $toRegionId
	 */
	public function setToRegionId($toRegionId)
	{
		$this->toRegionId = $toRegionId;
	}

	/**
	 * @return int
	 */
	public function getToRegionId()
	{
		return $this->toRegionId;
	}

	/**
	 * @param \Aeolun\MarketeerBundle\Entity\MapSolarSystems $toSolarSystem
	 */
	public function setToSolarSystem($toSolarSystem)
	{
		$this->toSolarSystem = $toSolarSystem;
	}

	/**
	 * @return \Aeolun\MarketeerBundle\Entity\MapSolarSystems
	 */
	public function getToSolarSystem()
	{
		return $this->toSolarSystem;
	}
}

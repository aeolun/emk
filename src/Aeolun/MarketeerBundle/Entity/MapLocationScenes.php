<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapLocationScenes
 *
 * @ORM\Table(name="map_location_scenes")
 * @ORM\Entity
 */
class MapLocationScenes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="location_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $locationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="graphic_id", type="integer", nullable=true)
     */
    private $graphicId;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapSolarSystems
 *
 * @ORM\Table(name="map_solar_system_distance")
 * @ORM\Entity
 */
class MapSolarSystemDistance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="solar_system_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $solarSystemId;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="to_solar_system_id", type="integer", nullable=true)
     */
    private $toSolarSystemId;

	/**
	 * @var boolean
	 *
	 * @ORM\Id
	 * @ORM\Column(name="is_safe", type="boolean", nullable=true)
	 */
	private $isSafe;

	/**
     * @var integer
     *
     * @ORM\Column(name="distance", type="integer", nullable=true)
     */
    private $distance;

	/**
	 * @param int $distance
	 */
	public function setDistance($distance)
	{
		$this->distance = $distance;
	}

	/**
	 * @return int
	 */
	public function getDistance()
	{
		return $this->distance;
	}

	/**
	 * @param boolean $isSafe
	 */
	public function setIsSafe($isSafe)
	{
		$this->isSafe = $isSafe;
	}

	/**
	 * @return boolean
	 */
	public function getIsSafe()
	{
		return $this->isSafe;
	}

	/**
	 * @param int $solarSystemId
	 */
	public function setSolarSystemId($solarSystemId)
	{
		$this->solarSystemId = $solarSystemId;
	}

	/**
	 * @return int
	 */
	public function getSolarSystemId()
	{
		return $this->solarSystemId;
	}

	/**
	 * @param int $toSolarSystemId
	 */
	public function setToSolarSystemId($toSolarSystemId)
	{
		$this->toSolarSystemId = $toSolarSystemId;
	}

	/**
	 * @return int
	 */
	public function getToSolarSystemId()
	{
		return $this->toSolarSystemId;
	}


}

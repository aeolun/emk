<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemMarketGroups
 *
 * @ORM\Table(name="item_market_groups")
 * @ORM\Entity
 */
class ItemMarketGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="market_group_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $marketGroupId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_group_id", type="integer", nullable=true)
     */
    private $parentGroupId;

    /**
     * @var string
     *
     * @ORM\Column(name="market_group_name", type="string", length=200, nullable=true)
     */
    private $marketGroupName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=6000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;

    /**
     * @var integer
     *
     * @ORM\Column(name="has_types", type="integer", nullable=true)
     */
    private $hasTypes;


}

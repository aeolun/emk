<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemContrabandTypes
 *
 * @ORM\Table(name="item_contraband_types", indexes={@Index(name="item_contraband_types_IX_type", columns={"type_id"})})
 * @ORM\Entity
 */
class ItemContrabandTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="faction_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $factionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $typeId;

    /**
     * @var float
     *
     * @ORM\Column(name="standing_loss", type="float", nullable=true)
     */
    private $standingLoss;

    /**
     * @var float
     *
     * @ORM\Column(name="confiscate_min_sec", type="float", nullable=true)
     */
    private $confiscateMinSec;

    /**
     * @var float
     *
     * @ORM\Column(name="fine_by_value", type="float", nullable=true)
     */
    private $fineByValue;

    /**
     * @var float
     *
     * @ORM\Column(name="attack_min_sec", type="float", nullable=true)
     */
    private $attackMinSec;


}

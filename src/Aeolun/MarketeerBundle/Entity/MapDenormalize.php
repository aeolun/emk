<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapDenormalize
 *
 * @ORM\Table(name="map_denormalize", indexes={@Index(name="map_denormalize_IX_group_region", columns={"group_id", "region_id"}), @Index(name="map_denormalize_IX_group_constellation", columns={"group_id", "constellation_id"}), @Index(name="map_denormalize_IX_group_system", columns={"group_id", "solar_system_id"}), @Index(name="map_denormalize_IX_system", columns={"solar_system_id"}), @Index(name="map_denormalize_IX_constellation", columns={"constellation_id"}), @Index(name="map_denormalize_IX_region", columns={"region_id"}), @Index(name="map_denormalize_IX_orbit", columns={"orbit_id"})})
 * @ORM\Entity
 */
class MapDenormalize
{
    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $itemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=true)
     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=true)
     */
    private $groupId;

    /**
     * @var integer
     *
     * @ORM\Column(name="solar_system_id", type="integer", nullable=true)
     */
    private $solarSystemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="constellation_id", type="integer", nullable=true)
     */
    private $constellationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="region_id", type="integer", nullable=true)
     */
    private $regionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="orbit_id", type="integer", nullable=true)
     */
    private $orbitId;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float", nullable=true)
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float", nullable=true)
     */
    private $y;

    /**
     * @var float
     *
     * @ORM\Column(name="z", type="float", nullable=true)
     */
    private $z;

    /**
     * @var float
     *
     * @ORM\Column(name="radius", type="float", nullable=true)
     */
    private $radius;

    /**
     * @var string
     *
     * @ORM\Column(name="item_name", type="string", length=200, nullable=true)
     */
    private $itemName;

    /**
     * @var float
     *
     * @ORM\Column(name="security", type="float", nullable=true)
     */
    private $security;

    /**
     * @var integer
     *
     * @ORM\Column(name="celestial_index", type="integer", nullable=true)
     */
    private $celestialIndex;

    /**
     * @var integer
     *
     * @ORM\Column(name="orbit_index", type="integer", nullable=true)
     */
    private $orbitIndex;

	/**
	 * @ORM\OneToMany(targetEntity="Orders", mappedBy="station")
	 */
	private $orders;

	/**
	 * @param int $celestialIndex
	 */
	public function setCelestialIndex($celestialIndex)
	{
		$this->celestialIndex = $celestialIndex;
	}

	/**
	 * @return int
	 */
	public function getCelestialIndex()
	{
		return $this->celestialIndex;
	}

	/**
	 * @param int $constellationId
	 */
	public function setConstellationId($constellationId)
	{
		$this->constellationId = $constellationId;
	}

	/**
	 * @return int
	 */
	public function getConstellationId()
	{
		return $this->constellationId;
	}

	/**
	 * @param int $groupId
	 */
	public function setGroupId($groupId)
	{
		$this->groupId = $groupId;
	}

	/**
	 * @return int
	 */
	public function getGroupId()
	{
		return $this->groupId;
	}

	/**
	 * @param int $itemId
	 */
	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
	}

	/**
	 * @return int
	 */
	public function getItemId()
	{
		return $this->itemId;
	}

	/**
	 * @param string $itemName
	 */
	public function setItemName($itemName)
	{
		$this->itemName = $itemName;
	}

	/**
	 * @return string
	 */
	public function getItemName()
	{
		return $this->itemName;
	}

	/**
	 * @param int $orbitId
	 */
	public function setOrbitId($orbitId)
	{
		$this->orbitId = $orbitId;
	}

	/**
	 * @return int
	 */
	public function getOrbitId()
	{
		return $this->orbitId;
	}

	/**
	 * @param int $orbitIndex
	 */
	public function setOrbitIndex($orbitIndex)
	{
		$this->orbitIndex = $orbitIndex;
	}

	/**
	 * @return int
	 */
	public function getOrbitIndex()
	{
		return $this->orbitIndex;
	}

	/**
	 * @param mixed $orders
	 */
	public function setOrders($orders)
	{
		$this->orders = $orders;
	}

	/**
	 * @return mixed
	 */
	public function getOrders()
	{
		return $this->orders;
	}

	/**
	 * @param float $radius
	 */
	public function setRadius($radius)
	{
		$this->radius = $radius;
	}

	/**
	 * @return float
	 */
	public function getRadius()
	{
		return $this->radius;
	}

	/**
	 * @param int $regionId
	 */
	public function setRegionId($regionId)
	{
		$this->regionId = $regionId;
	}

	/**
	 * @return int
	 */
	public function getRegionId()
	{
		return $this->regionId;
	}

	/**
	 * @param float $security
	 */
	public function setSecurity($security)
	{
		$this->security = $security;
	}

	/**
	 * @return float
	 */
	public function getSecurity()
	{
		return $this->security;
	}

	/**
	 * @param int $solarSystemId
	 */
	public function setSolarSystemId($solarSystemId)
	{
		$this->solarSystemId = $solarSystemId;
	}

	/**
	 * @return int
	 */
	public function getSolarSystemId()
	{
		return $this->solarSystemId;
	}

	/**
	 * @param int $typeId
	 */
	public function setTypeId($typeId)
	{
		$this->typeId = $typeId;
	}

	/**
	 * @return int
	 */
	public function getTypeId()
	{
		return $this->typeId;
	}

	/**
	 * @param float $x
	 */
	public function setX($x)
	{
		$this->x = $x;
	}

	/**
	 * @return float
	 */
	public function getX()
	{
		return $this->x;
	}

	/**
	 * @param float $y
	 */
	public function setY($y)
	{
		$this->y = $y;
	}

	/**
	 * @return float
	 */
	public function getY()
	{
		return $this->y;
	}

	/**
	 * @param float $z
	 */
	public function setZ($z)
	{
		$this->z = $z;
	}

	/**
	 * @return float
	 */
	public function getZ()
	{
		return $this->z;
	}


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * IndustrialAssemblyLineTypes
 *
 * @ORM\Table(name="industrial_assembly_line_types")
 * @ORM\Entity
 */
class IndustrialAssemblyLineTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="assembly_line_type_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $assemblyLineTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="assembly_line_type_name", type="string", length=200, nullable=true)
     */
    private $assemblyLineTypeName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="base_time_multiplier", type="float", nullable=true)
     */
    private $baseTimeMultiplier;

    /**
     * @var float
     *
     * @ORM\Column(name="base_material_multiplier", type="float", nullable=true)
     */
    private $baseMaterialMultiplier;

    /**
     * @var float
     *
     * @ORM\Column(name="volume", type="float", nullable=true)
     */
    private $volume;

    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="integer", nullable=true)
     */
    private $activityId;

    /**
     * @var float
     *
     * @ORM\Column(name="min_cost_per_hour", type="float", nullable=true)
     */
    private $minCostPerHour;


}

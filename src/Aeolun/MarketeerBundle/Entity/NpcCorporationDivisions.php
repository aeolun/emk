<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * NpcCorporationDivisions
 *
 * @ORM\Table(name="npc_corporation_divisions")
 * @ORM\Entity
 */
class NpcCorporationDivisions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="corporation_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $corporationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="division_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $divisionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    private $size;


}

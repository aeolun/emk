<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ResearchAgents
 *
 * @ORM\Table(name="research_agents", indexes={@Index(name="research_agents_IX_type", columns={"type_id"})})
 * @ORM\Entity
 */
class ResearchAgents
{
    /**
     * @var integer
     *
     * @ORM\Column(name="agent_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $agentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $typeId;


}

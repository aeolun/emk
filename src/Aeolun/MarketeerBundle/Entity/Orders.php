<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * Orders
 *
 * @ORM\Table(name="orders", indexes={@Index(name="IX_type_region", columns={"type_id", "region_id"})})
 * @ORM\Entity
 */
class Orders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

	/**
	 * @var ItemType $type
	 *
	 * @ORM\ManyToOne(targetEntity="ItemType", inversedBy="orders")
	 * @ORM\JoinColumn(name="type_id", referencedColumnName="type_id", onDelete="CASCADE")
	 */
	private $type;

	/**
	 * @var MapRegion $region
	 *
	 * @ORM\ManyToOne(targetEntity="MapRegion", inversedBy="orders")
	 * @ORM\JoinColumn(name="region_id", referencedColumnName="region_id", onDelete="CASCADE")
	 */
	private $region;

    /**
     * @var integer
     *
     * @ORM\Column(name="solar_system_id", type="integer", nullable=false)
     */
    private $solarSystemId;

	/**
	 * @var MapDenormalize $station
	 *
	 * @ORM\ManyToOne(targetEntity="MapDenormalize", inversedBy="orders", fetch="EAGER")
	 * @ORM\JoinColumn(name="station_id", referencedColumnName="item_id", onDelete="CASCADE")
	 */
	private $station;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=20, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="volume_remaining", type="integer", nullable=false)
     */
    private $volumeRemaining;

    /**
     * @var integer
     *
     * @ORM\Column(name="volume_entered", type="integer", nullable=false)
     */
    private $volumeEntered;

    /**
     * @var integer
     *
     * @ORM\Column(name="volume_minimum", type="integer", nullable=false)
     */
    private $volumeMinimum;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_bid", type="integer", nullable=false)
     */
    private $isBid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="issued", type="datetime", nullable=false)
     */
    private $issued;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer", nullable=false)
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="bullshit", type="integer", nullable=false)
     */
    private $bullshit;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated", type="integer", nullable=false)
     */
    private $updated;

	/**
	 * @param int $bullshit
	 */
	public function setBullshit($bullshit)
	{
		$this->bullshit = $bullshit;
	}

	/**
	 * @return int
	 */
	public function getBullshit()
	{
		return $this->bullshit;
	}

	/**
	 * @param int $duration
	 */
	public function setDuration($duration)
	{
		$this->duration = $duration;
	}

	/**
	 * @return int
	 */
	public function getDuration()
	{
		return $this->duration;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $isBid
	 */
	public function setIsBid($isBid)
	{
		$this->isBid = $isBid;
	}

	/**
	 * @return int
	 */
	public function getIsBid()
	{
		return $this->isBid;
	}

	/**
	 * @param \DateTime $issued
	 */
	public function setIssued($issued)
	{
		$this->issued = $issued;
	}

	/**
	 * @return \DateTime
	 */
	public function getIssued()
	{
		return $this->issued;
	}

	/**
	 * @param float $price
	 */
	public function setPrice($price)
	{
		$this->price = $price;
	}

	/**
	 * @return float
	 */
	public function getPrice()
	{
		return $this->price;
	}

	/**
	 * @param int $solarSystemId
	 */
	public function setSolarSystemId($solarSystemId)
	{
		$this->solarSystemId = $solarSystemId;
	}

	/**
	 * @return int
	 */
	public function getSolarSystemId()
	{
		return $this->solarSystemId;
	}

	/**
	 * @param \Aeolun\MarketeerBundle\Entity\MapDenormalize $station
	 */
	public function setStation($station)
	{
		$this->station = $station;
	}

	/**
	 * @return \Aeolun\MarketeerBundle\Entity\MapDenormalize
	 */
	public function getStation()
	{
		return $this->station;
	}

	/**
	 * @param \Aeolun\MarketeerBundle\Entity\MapRegion $region
	 */
	public function setRegion($region)
	{
		$this->region = $region;
	}

	/**
	 * @return \Aeolun\MarketeerBundle\Entity\MapRegion
	 */
	public function getRegion()
	{
		return $this->region;
	}


	/**
	 * @param \Aeolun\MarketeerBundle\Entity\ItemType $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return \Aeolun\MarketeerBundle\Entity\ItemType
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param int $volumeEntered
	 */
	public function setVolumeEntered($volumeEntered)
	{
		$this->volumeEntered = $volumeEntered;
	}

	/**
	 * @return int
	 */
	public function getVolumeEntered()
	{
		return $this->volumeEntered;
	}

	/**
	 * @param int $volumeMinimum
	 */
	public function setVolumeMinimum($volumeMinimum)
	{
		$this->volumeMinimum = $volumeMinimum;
	}

	/**
	 * @return int
	 */
	public function getVolumeMinimum()
	{
		return $this->volumeMinimum;
	}

	/**
	 * @param int $volumeRemaining
	 */
	public function setVolumeRemaining($volumeRemaining)
	{
		$this->volumeRemaining = $volumeRemaining;
	}

	/**
	 * @return int
	 */
	public function getVolumeRemaining()
	{
		return $this->volumeRemaining;
	}


}

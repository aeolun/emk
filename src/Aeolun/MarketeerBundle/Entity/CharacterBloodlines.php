<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * CharacterBloodlines
 *
 * @ORM\Table(name="character_bloodlines")
 * @ORM\Entity
 */
class CharacterBloodlines
{
    /**
     * @var integer
     *
     * @ORM\Column(name="bloodline_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $bloodlineId;

    /**
     * @var string
     *
     * @ORM\Column(name="bloodline_name", type="string", length=200, nullable=true)
     */
    private $bloodlineName;

    /**
     * @var integer
     *
     * @ORM\Column(name="race_id", type="integer", nullable=true)
     */
    private $raceId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="male_description", type="string", length=2000, nullable=true)
     */
    private $maleDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="female_description", type="string", length=2000, nullable=true)
     */
    private $femaleDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="ship_type_id", type="integer", nullable=true)
     */
    private $shipTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="corporation_id", type="integer", nullable=true)
     */
    private $corporationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="perception", type="integer", nullable=true)
     */
    private $perception;

    /**
     * @var integer
     *
     * @ORM\Column(name="willpower", type="integer", nullable=true)
     */
    private $willpower;

    /**
     * @var integer
     *
     * @ORM\Column(name="charisma", type="integer", nullable=true)
     */
    private $charisma;

    /**
     * @var integer
     *
     * @ORM\Column(name="memory", type="integer", nullable=true)
     */
    private $memory;

    /**
     * @var integer
     *
     * @ORM\Column(name="intelligence", type="integer", nullable=true)
     */
    private $intelligence;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="string", length=1000, nullable=true)
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="short_male_description", type="string", length=1000, nullable=true)
     */
    private $shortMaleDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="short_female_description", type="string", length=1000, nullable=true)
     */
    private $shortFemaleDescription;


}

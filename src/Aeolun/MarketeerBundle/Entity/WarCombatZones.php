<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * WarCombatZones
 *
 * @ORM\Table(name="war_combat_zones")
 * @ORM\Entity
 */
class WarCombatZones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="combat_zone_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $combatZoneId;

    /**
     * @var string
     *
     * @ORM\Column(name="combat_zone_name", type="string", length=200, nullable=true)
     */
    private $combatZoneName;

    /**
     * @var integer
     *
     * @ORM\Column(name="faction_id", type="integer", nullable=true)
     */
    private $factionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="center_system_id", type="integer", nullable=true)
     */
    private $centerSystemId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapLandmarks
 *
 * @ORM\Table(name="map_landmarks")
 * @ORM\Entity
 */
class MapLandmarks
{
    /**
     * @var integer
     *
     * @ORM\Column(name="landmark_id", type="smallint", nullable=false)
     * @ORM\Id

     */
    private $landmarkId;

    /**
     * @var string
     *
     * @ORM\Column(name="landmark_name", type="string", length=100, nullable=true)
     */
    private $landmarkName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=7000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="location_id", type="integer", nullable=true)
     */
    private $locationId;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float", nullable=true)
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float", nullable=true)
     */
    private $y;

    /**
     * @var float
     *
     * @ORM\Column(name="z", type="float", nullable=true)
     */
    private $z;

    /**
     * @var float
     *
     * @ORM\Column(name="radius", type="float", nullable=true)
     */
    private $radius;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;

    /**
     * @var integer
     *
     * @ORM\Column(name="importance", type="integer", nullable=true)
     */
    private $importance;


}

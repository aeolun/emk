<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemControlTowerResourcePurposes
 *
 * @ORM\Table(name="item_control_tower_resource_purposes")
 * @ORM\Entity
 */
class ItemControlTowerResourcePurposes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="purpose", type="integer", nullable=false)
     * @ORM\Id

     */
    private $purpose;

    /**
     * @var string
     *
     * @ORM\Column(name="purpose_text", type="string", length=100, nullable=true)
     */
    private $purposeText;


}

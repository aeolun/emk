<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * NpcCorporationResearchFields
 *
 * @ORM\Table(name="npc_corporation_research_fields")
 * @ORM\Entity
 */
class NpcCorporationResearchFields
{
    /**
     * @var integer
     *
     * @ORM\Column(name="skill_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $skillId;

    /**
     * @var integer
     *
     * @ORM\Column(name="corporation_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $corporationId;


}

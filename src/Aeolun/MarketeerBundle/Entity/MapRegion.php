<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapRegion
 *
 * @ORM\Table(name="map_regions")
 * @ORM\Entity
 */
class MapRegion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="region_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $regionId;

    /**
     * @var string
     *
     * @ORM\Column(name="region_name", type="string", length=200, nullable=true)
     */
    private $regionName;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float", nullable=true)
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float", nullable=true)
     */
    private $y;

    /**
     * @var float
     *
     * @ORM\Column(name="z", type="float", nullable=true)
     */
    private $z;

    /**
     * @var float
     *
     * @ORM\Column(name="x_min", type="float", nullable=true)
     */
    private $xMin;

    /**
     * @var float
     *
     * @ORM\Column(name="x_max", type="float", nullable=true)
     */
    private $xMax;

    /**
     * @var float
     *
     * @ORM\Column(name="y_min", type="float", nullable=true)
     */
    private $yMin;

    /**
     * @var float
     *
     * @ORM\Column(name="y_max", type="float", nullable=true)
     */
    private $yMax;

    /**
     * @var float
     *
     * @ORM\Column(name="z_min", type="float", nullable=true)
     */
    private $zMin;

    /**
     * @var float
     *
     * @ORM\Column(name="z_max", type="float", nullable=true)
     */
    private $zMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="faction_id", type="integer", nullable=true)
     */
    private $factionId;

    /**
     * @var float
     *
     * @ORM\Column(name="radius", type="float", nullable=true)
     */
    private $radius;

	/**
	 * @ORM\ManyToMany(targetEntity="User", mappedBy="regions")
	 */
	private $users;

	/**
	 * @ORM\OneToMany(targetEntity="Orders", mappedBy="region")
	 */
	private $orders;

	/**
	 * @ORM\OneToMany(targetEntity="User", mappedBy="primaryRegion", fetch="EXTRA_LAZY")
	 */
	private $primaryUsers;

	/**
	 * @ORM\OneToMany(targetEntity="History", mappedBy="region", fetch="EXTRA_LAZY")
	 */
	private $history;

	/**
	 * @param int $factionId
	 */
	public function setFactionId($factionId)
	{
		$this->factionId = $factionId;
	}

	/**
	 * @return int
	 */
	public function getFactionId()
	{
		return $this->factionId;
	}

	/**
	 * @param float $radius
	 */
	public function setRadius($radius)
	{
		$this->radius = $radius;
	}

	/**
	 * @return float
	 */
	public function getRadius()
	{
		return $this->radius;
	}

	/**
	 * @param int $regionId
	 */
	public function setRegionId($regionId)
	{
		$this->regionId = $regionId;
	}

	/**
	 * @return int
	 */
	public function getRegionId()
	{
		return $this->regionId;
	}

	/**
	 * @param string $regionName
	 */
	public function setRegionName($regionName)
	{
		$this->regionName = $regionName;
	}

	/**
	 * @return string
	 */
	public function getRegionName()
	{
		return $this->regionName;
	}

	/**
	 * @param mixed $users
	 */
	public function setUsers($users)
	{
		$this->users = $users;
	}

	/**
	 * @return mixed
	 */
	public function getUsers()
	{
		return $this->users;
	}

	/**
	 * @param float $x
	 */
	public function setX($x)
	{
		$this->x = $x;
	}

	/**
	 * @return float
	 */
	public function getX()
	{
		return $this->x;
	}

	/**
	 * @param float $xMax
	 */
	public function setXMax($xMax)
	{
		$this->xMax = $xMax;
	}

	/**
	 * @return float
	 */
	public function getXMax()
	{
		return $this->xMax;
	}

	/**
	 * @param float $xMin
	 */
	public function setXMin($xMin)
	{
		$this->xMin = $xMin;
	}

	/**
	 * @return float
	 */
	public function getXMin()
	{
		return $this->xMin;
	}

	/**
	 * @param float $y
	 */
	public function setY($y)
	{
		$this->y = $y;
	}

	/**
	 * @return float
	 */
	public function getY()
	{
		return $this->y;
	}

	/**
	 * @param float $yMax
	 */
	public function setYMax($yMax)
	{
		$this->yMax = $yMax;
	}

	/**
	 * @return float
	 */
	public function getYMax()
	{
		return $this->yMax;
	}

	/**
	 * @param float $yMin
	 */
	public function setYMin($yMin)
	{
		$this->yMin = $yMin;
	}

	/**
	 * @return float
	 */
	public function getYMin()
	{
		return $this->yMin;
	}

	/**
	 * @param float $z
	 */
	public function setZ($z)
	{
		$this->z = $z;
	}

	/**
	 * @return float
	 */
	public function getZ()
	{
		return $this->z;
	}

	/**
	 * @param float $zMax
	 */
	public function setZMax($zMax)
	{
		$this->zMax = $zMax;
	}

	/**
	 * @return float
	 */
	public function getZMax()
	{
		return $this->zMax;
	}

	/**
	 * @param float $zMin
	 */
	public function setZMin($zMin)
	{
		$this->zMin = $zMin;
	}

	/**
	 * @return float
	 */
	public function getZMin()
	{
		return $this->zMin;
	}


}

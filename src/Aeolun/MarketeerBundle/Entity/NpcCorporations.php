<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * NpcCorporations
 *
 * @ORM\Table(name="npc_corporations")
 * @ORM\Entity
 */
class NpcCorporations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="corporation_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $corporationId;

    /**
     * @var string
     *
     * @ORM\Column(name="size", type="string", length=1, nullable=true)
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="extent", type="string", length=1, nullable=true)
     */
    private $extent;

    /**
     * @var integer
     *
     * @ORM\Column(name="solar_system_id", type="integer", nullable=true)
     */
    private $solarSystemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="investor_id1", type="integer", nullable=true)
     */
    private $investorId1;

    /**
     * @var integer
     *
     * @ORM\Column(name="investor_shares1", type="integer", nullable=true)
     */
    private $investorShares1;

    /**
     * @var integer
     *
     * @ORM\Column(name="investor_id2", type="integer", nullable=true)
     */
    private $investorId2;

    /**
     * @var integer
     *
     * @ORM\Column(name="investor_shares2", type="integer", nullable=true)
     */
    private $investorShares2;

    /**
     * @var integer
     *
     * @ORM\Column(name="investor_id3", type="integer", nullable=true)
     */
    private $investorId3;

    /**
     * @var integer
     *
     * @ORM\Column(name="investor_shares3", type="integer", nullable=true)
     */
    private $investorShares3;

    /**
     * @var integer
     *
     * @ORM\Column(name="investor_id4", type="integer", nullable=true)
     */
    private $investorId4;

    /**
     * @var integer
     *
     * @ORM\Column(name="investor_shares4", type="integer", nullable=true)
     */
    private $investorShares4;

    /**
     * @var integer
     *
     * @ORM\Column(name="friend_id", type="integer", nullable=true)
     */
    private $friendId;

    /**
     * @var integer
     *
     * @ORM\Column(name="enemy_id", type="integer", nullable=true)
     */
    private $enemyId;

    /**
     * @var integer
     *
     * @ORM\Column(name="public_shares", type="bigint", nullable=true)
     */
    private $publicShares;

    /**
     * @var integer
     *
     * @ORM\Column(name="initial_price", type="integer", nullable=true)
     */
    private $initialPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="min_security", type="float", nullable=true)
     */
    private $minSecurity;

    /**
     * @var integer
     *
     * @ORM\Column(name="scattered", type="integer", nullable=true)
     */
    private $scattered;

    /**
     * @var integer
     *
     * @ORM\Column(name="fringe", type="integer", nullable=true)
     */
    private $fringe;

    /**
     * @var integer
     *
     * @ORM\Column(name="corridor", type="integer", nullable=true)
     */
    private $corridor;

    /**
     * @var integer
     *
     * @ORM\Column(name="hub", type="integer", nullable=true)
     */
    private $hub;

    /**
     * @var integer
     *
     * @ORM\Column(name="border", type="integer", nullable=true)
     */
    private $border;

    /**
     * @var integer
     *
     * @ORM\Column(name="faction_id", type="integer", nullable=true)
     */
    private $factionId;

    /**
     * @var float
     *
     * @ORM\Column(name="size_factor", type="float", nullable=true)
     */
    private $sizeFactor;

    /**
     * @var integer
     *
     * @ORM\Column(name="station_count", type="smallint", nullable=true)
     */
    private $stationCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="station_system_count", type="smallint", nullable=true)
     */
    private $stationSystemCount;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=8000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;


}

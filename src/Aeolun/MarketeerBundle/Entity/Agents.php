<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * Agents
 *
 * @ORM\Table(name="agents", indexes={@Index(name="agents_IX_corporation", columns={"corporation_id"}), @Index(name="agents_IX_station", columns={"location_id"})})
 * @ORM\Entity
 */
class Agents
{
    /**
     * @var integer
     *
     * @ORM\Column(name="agent_id", type="integer", nullable=false)
     * @ORM\Id
     */
    private $agentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="division_id", type="integer", nullable=true)
     */
    private $divisionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="corporation_id", type="integer", nullable=true)
     */
    private $corporationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="location_id", type="integer", nullable=true)
     */
    private $locationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level;

    /**
     * @var integer
     *
     * @ORM\Column(name="quality", type="smallint", nullable=true)
     */
    private $quality;

    /**
     * @var integer
     *
     * @ORM\Column(name="agent_type_id", type="integer", nullable=true)
     */
    private $agentTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_locator", type="integer", nullable=true)
     */
    private $isLocator;


}

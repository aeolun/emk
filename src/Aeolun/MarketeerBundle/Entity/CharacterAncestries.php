<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * CharacterAncestries
 *
 * @ORM\Table(name="character_ancestries")
 * @ORM\Entity
 */
class CharacterAncestries
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ancestry_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $ancestryId;

    /**
     * @var string
     *
     * @ORM\Column(name="ancestry_name", type="string", length=200, nullable=true)
     */
    private $ancestryName;

    /**
     * @var integer
     *
     * @ORM\Column(name="bloodline_id", type="integer", nullable=true)
     */
    private $bloodlineId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="perception", type="integer", nullable=true)
     */
    private $perception;

    /**
     * @var integer
     *
     * @ORM\Column(name="willpower", type="integer", nullable=true)
     */
    private $willpower;

    /**
     * @var integer
     *
     * @ORM\Column(name="charisma", type="integer", nullable=true)
     */
    private $charisma;

    /**
     * @var integer
     *
     * @ORM\Column(name="memory", type="integer", nullable=true)
     */
    private $memory;

    /**
     * @var integer
     *
     * @ORM\Column(name="intelligence", type="integer", nullable=true)
     */
    private $intelligence;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="string", length=1000, nullable=true)
     */
    private $shortDescription;


}

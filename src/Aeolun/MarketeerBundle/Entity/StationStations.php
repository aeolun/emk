<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * StationStations
 *
 * @ORM\Table(name="station_stations", indexes={@Index(name="station_stations_IX_region", columns={"region_id"}), @Index(name="station_stations_IX_system", columns={"solar_system_id"}), @Index(name="station_stations_IX_constellation", columns={"constellation_id"}), @Index(name="station_stations_IX_operation", columns={"operation_id"}), @Index(name="station_stations_IX_type", columns={"station_type_id"}), @Index(name="station_stations_IX_corporation", columns={"corporation_id"})})
 * @ORM\Entity
 */
class StationStations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="station_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $stationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="security", type="smallint", nullable=true)
     */
    private $security;

    /**
     * @var float
     *
     * @ORM\Column(name="docking_cost_per_volume", type="float", nullable=true)
     */
    private $dockingCostPerVolume;

    /**
     * @var float
     *
     * @ORM\Column(name="max_ship_volume_dockable", type="float", nullable=true)
     */
    private $maxShipVolumeDockable;

    /**
     * @var integer
     *
     * @ORM\Column(name="office_rental_cost", type="integer", nullable=true)
     */
    private $officeRentalCost;

    /**
     * @var integer
     *
     * @ORM\Column(name="operation_id", type="integer", nullable=true)
     */
    private $operationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="station_type_id", type="integer", nullable=true)
     */
    private $stationTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="corporation_id", type="integer", nullable=true)
     */
    private $corporationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="solar_system_id", type="integer", nullable=true)
     */
    private $solarSystemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="constellation_id", type="integer", nullable=true)
     */
    private $constellationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="region_id", type="integer", nullable=true)
     */
    private $regionId;

    /**
     * @var string
     *
     * @ORM\Column(name="station_name", type="string", length=200, nullable=true)
     */
    private $stationName;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float", nullable=true)
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float", nullable=true)
     */
    private $y;

    /**
     * @var float
     *
     * @ORM\Column(name="z", type="float", nullable=true)
     */
    private $z;

    /**
     * @var float
     *
     * @ORM\Column(name="reprocessing_efficiency", type="float", nullable=true)
     */
    private $reprocessingEfficiency;

    /**
     * @var float
     *
     * @ORM\Column(name="reprocessing_stations_take", type="float", nullable=true)
     */
    private $reprocessingStationsTake;

    /**
     * @var integer
     *
     * @ORM\Column(name="reprocessing_hangar_flag", type="integer", nullable=true)
     */
    private $reprocessingHangarFlag;


}

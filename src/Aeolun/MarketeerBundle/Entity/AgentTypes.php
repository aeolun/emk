<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * AgentTypes
 *
 * @ORM\Table(name="agent_types")
 * @ORM\Entity
 */
class AgentTypes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="agent_type_id", type="integer", nullable=false)
     * @ORM\Id
     */
    private $agentTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="agent_type", type="string", length=50, nullable=true)
     */
    private $agentType;


}

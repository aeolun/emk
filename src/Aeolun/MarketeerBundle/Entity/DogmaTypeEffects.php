<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * DogmaTypeEffects
 *
 * @ORM\Table(name="dogma_type_effects")
 * @ORM\Entity
 */
class DogmaTypeEffects
{
    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="effect_id", type="smallint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $effectId;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_default", type="integer", nullable=true)
     */
    private $isDefault;


}

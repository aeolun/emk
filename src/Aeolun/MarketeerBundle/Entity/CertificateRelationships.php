<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * CertificateRelationships
 *
 * @ORM\Table(name="certificate_relationships", indexes={@Index(name="certificate_relationships_IX_parent", columns={"parent_id"}), @Index(name="certificate_relationships_IX_child", columns={"child_id"})})
 * @ORM\Entity
 */
class CertificateRelationships
{
    /**
     * @var integer
     *
     * @ORM\Column(name="relationship_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $relationshipId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_type_id", type="integer", nullable=true)
     */
    private $parentTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_level", type="integer", nullable=true)
     */
    private $parentLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="child_id", type="integer", nullable=true)
     */
    private $childId;


}

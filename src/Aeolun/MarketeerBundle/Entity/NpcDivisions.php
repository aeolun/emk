<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * NpcDivisions
 *
 * @ORM\Table(name="npc_divisions")
 * @ORM\Entity
 */
class NpcDivisions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="division_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $divisionId;

    /**
     * @var string
     *
     * @ORM\Column(name="division_name", type="string", length=200, nullable=true)
     */
    private $divisionName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="leader_type", type="string", length=200, nullable=true)
     */
    private $leaderType;


}

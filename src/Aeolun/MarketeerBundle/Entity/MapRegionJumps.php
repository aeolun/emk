<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapRegionJumps
 *
 * @ORM\Table(name="map_region_jumps")
 * @ORM\Entity
 */
class MapRegionJumps
{
    /**
     * @var integer
     *
     * @ORM\Column(name="from_region_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $fromRegionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="to_region_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $toRegionId;


}

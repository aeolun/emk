<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemNames
 *
 * @ORM\Table(name="item_names")
 * @ORM\Entity
 */
class ItemNames
{
    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="bigint", nullable=false)
     * @ORM\Id

     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="item_name", type="string", length=400, nullable=false)
     */
    private $itemName;


}

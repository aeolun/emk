<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapConstellations
 *
 * @ORM\Table(name="map_constellations", indexes={@Index(name="map_constellations_IX_region", columns={"region_id"})})
 * @ORM\Entity
 */
class MapConstellations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="constellation_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $constellationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="region_id", type="integer", nullable=true)
     */
    private $regionId;

    /**
     * @var string
     *
     * @ORM\Column(name="constellation_name", type="string", length=200, nullable=true)
     */
    private $constellationName;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float", nullable=true)
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float", nullable=true)
     */
    private $y;

    /**
     * @var float
     *
     * @ORM\Column(name="z", type="float", nullable=true)
     */
    private $z;

    /**
     * @var float
     *
     * @ORM\Column(name="x_min", type="float", nullable=true)
     */
    private $xMin;

    /**
     * @var float
     *
     * @ORM\Column(name="x_max", type="float", nullable=true)
     */
    private $xMax;

    /**
     * @var float
     *
     * @ORM\Column(name="y_min", type="float", nullable=true)
     */
    private $yMin;

    /**
     * @var float
     *
     * @ORM\Column(name="y_max", type="float", nullable=true)
     */
    private $yMax;

    /**
     * @var float
     *
     * @ORM\Column(name="z_min", type="float", nullable=true)
     */
    private $zMin;

    /**
     * @var float
     *
     * @ORM\Column(name="z_max", type="float", nullable=true)
     */
    private $zMax;

    /**
     * @var integer
     *
     * @ORM\Column(name="faction_id", type="integer", nullable=true)
     */
    private $factionId;

    /**
     * @var float
     *
     * @ORM\Column(name="radius", type="float", nullable=true)
     */
    private $radius;


}

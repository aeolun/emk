<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;
use Doctrine\ORM\Mapping\UniqueConstraint as UniqueConstraint;

/**
 * ItemUniqueNames
 *
 * @ORM\Table(name="item_unique_names", uniqueConstraints={@UniqueConstraint(name="item_unique_names_UQ",columns={"item_name"})}, indexes={@Index(name="item_unique_names_IX_Group_name", columns={"group_id", "item_name"})})
 * @ORM\Entity
 */
class ItemUniqueNames
{
    /**
     * @var integer
     *
     * @ORM\Column(name="item_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $itemId;

    /**
     * @var string
     *
     * @ORM\Column(name="item_name", type="string", length=400, nullable=false)
     */
    private $itemName;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=true)
     */
    private $groupId;


}

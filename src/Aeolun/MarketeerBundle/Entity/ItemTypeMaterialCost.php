<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemTypeMaterialCost
 *
 * @ORM\Table(name="item_type_material_cost")
 * @ORM\Entity
 */
class ItemTypeMaterialCost
{
    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="region_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $regionId;

    /**
     * @var float
     *
     * @ORM\Column(name="base_mineral_cost", type="decimal", nullable=false)
     */
    private $baseMineralCost;

    /**
     * @var float
     *
     * @ORM\Column(name="base_component_cost", type="decimal", nullable=false)
     */
    private $baseComponentCost;

    /**
     * @var float
     *
     * @ORM\Column(name="base_mineral_volume", type="decimal", nullable=false)
     */
    private $baseMineralVolume;

    /**
     * @var float
     *
     * @ORM\Column(name="base_component_volume", type="decimal", nullable=false)
     */
    private $baseComponentVolume;


}

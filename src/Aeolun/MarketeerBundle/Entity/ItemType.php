<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemType
 *
 * @ORM\Table(name="item_types", indexes={@Index(name="item_types_IX_Group", columns={"group_id"})})
 * @ORM\Entity
 */
class ItemType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=true)
     */
    private $groupId;

    /**
     * @var string
     *
     * @ORM\Column(name="type_name", type="string", length=200, nullable=true)
     */
    private $typeName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=6000, nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="mass", type="float", nullable=true)
     */
    private $mass;

    /**
     * @var float
     *
     * @ORM\Column(name="volume", type="float", nullable=true)
     */
    private $volume;

    /**
     * @var float
     *
     * @ORM\Column(name="capacity", type="float", nullable=true)
     */
    private $capacity;

    /**
     * @var integer
     *
     * @ORM\Column(name="portion_size", type="integer", nullable=true)
     */
    private $portionSize;

    /**
     * @var integer
     *
     * @ORM\Column(name="race_id", type="integer", nullable=true)
     */
    private $raceId;

    /**
     * @var float
     *
     * @ORM\Column(name="base_price", type="decimal", nullable=true)
     */
    private $basePrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="published", type="integer", nullable=true)
     */
    private $published;

    /**
     * @var integer
     *
     * @ORM\Column(name="market_group_id", type="integer", nullable=true)
     */
    private $marketGroupId;

    /**
     * @var float
     *
     * @ORM\Column(name="chance_of_duplicating", type="float", nullable=true)
     */
    private $chanceOfDuplicating;

	/**
	 * @ORM\OneToMany(targetEntity="Orders", mappedBy="type")
	 */
	private $orders;

	/**
	 * @ORM\OneToMany(targetEntity="StatisticalInfo", mappedBy="type", fetch="EXTRA_LAZY")
	 */
	private $statistics;

	/**
	 * @var History[] history
	 *
	 * @ORM\OneToMany(targetEntity="History", mappedBy="type", fetch="EXTRA_LAZY")
	 */
	private $history;

	function __construct() {
		$this->orders = new ArrayCollection();
		$this->statistics = new ArrayCollection();
		$this->history = new ArrayCollection();
	}

	public function getStatisticsByRegion(MapRegion $region) {
		$c = Criteria::create();
		$c->where($c->expr()->eq('region', $region));
		return $this->getStatistics()->matching($c)->first();
	}

	public function getStatisticsByRegions($regions) {
		$c = Criteria::create();
		$c->where($c->expr()->in('region', $regions));
		return $this->getStatistics()->matching($c);
	}

	/**
	 * @param MapRegion $region
	 * @return History[]
	 */
	public function getHistoryByRegion(MapRegion $region) {
		$c = Criteria::create();
		$c->where($c->expr()->eq('region', $region));

		return $this->getHistory()->matching($c);
	}

	public function getOrdersByRegion(MapRegion $region, $sort = null) {
		$c = Criteria::create();
		$c->where($c->expr()->eq('region', $region));
		if ($sort) $c->orderBy($sort);
		return $this->getOrders()->matching($c);
	}

	public function getHistoryTrendByRegion(MapRegion $region, $flat = false) {
		$history = $this->getHistoryByRegion($region);

		$data = array();
		$lastDay = null;

		foreach($history as $day) {
			//if ($day->getDate() < new \DateTime('-2 year')) continue;
			$row = array(
				'date' => $day->getDate()->getTimestamp()*1000,
				'begin' => $lastDay?$lastDay->getAverage():$day->getAverage(),
				'max' => $day->getHigh(),
				'min' => $day->getLow(),
				'end' => $day->getAverage()
			);
			if ($flat) {
				$data[] = array_values($row);
			} else {
				$data[] = $row;
			}
			$lastDay = $day;
		}
		return $data;
	}

	public function getHistoryGraphByRegion(MapRegion $region, $flat = false) {
		$history = $this->getHistoryByRegion($region);

		$data = array();
		$last_x = array();
		$don_default_max = null;
		$don_default_min = null;
		$default_a5 = null;
		$default_a20 = null;

		foreach($history as $day) {
			//if ($day->getDate() < new \DateTime('-6 month')) continue;

			$last_x[] = $day->getAverage();
			$don_max[] = $day->getHigh();
			$don_min[] = $day->getLow();
			$don_count = count($don_max);
			if ($don_count >= 7) {
				$don = array_slice(array_values($don_max),$don_count-7,7);
				$donchian_max = max($don);
				$don = array_slice(array_values($don_min),$don_count-7,7);
				$donchian_min = min($don);
				if($don_default_max == null) $don_default_max = $donchian_max;
				if($don_default_min == null) $don_default_min = $donchian_min;
			} else {
				$donchian_max = max(array_values($don_max));
				$donchian_min = min(array_values($don_min));
			}
			if (count($last_x) >= 5) {
				$moving_average_5 = array_sum(array_slice(array_values($last_x),count($last_x)-5,5)) / 5;
				if($default_a5 == null) $default_a5 = $moving_average_5;
			} else {
				$moving_average_5 = null;
			}
			if (count($last_x) == 20) {
				$moving_average_20 = array_sum($last_x) / 20;
				if($default_a20 == null) $default_a20 = $moving_average_20;
				array_shift($last_x);
			} else {
				$moving_average_20 = null;
			}

			$row = array(
				'date' => $day->getDate()->getTimestamp()*1000,
				'avg' => $day->getAverage(),
				'moving_5' => $moving_average_5,
				'moving_20' => $moving_average_20,
				'donchian_min' => $donchian_min,
				'donchian_max' => $donchian_max,
				'min' => $day->getLow(),
				'max' => $day->getHigh()
			);
			if ($flat) {
				$data[] = array_values($row);
			} else {
				$data[] = $row;
			}
		}
		return $data;
	}

	/**
	 * @param float $basePrice
	 */
	public function setBasePrice($basePrice)
	{
		$this->basePrice = $basePrice;
	}

	/**
	 * @return float
	 */
	public function getBasePrice()
	{
		return $this->basePrice;
	}

	/**
	 * @param ArrayCollection $orders
	 */
	public function setOrders($orders)
	{
		$this->orders = $orders;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getOrders()
	{
		return $this->orders;
	}

	/**
	 * @param ArrayCollection $statistics
	 */
	public function setStatistics($statistics)
	{
		$this->statistics = $statistics;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getStatistics()
	{
		return $this->statistics;
	}



	/**
	 * @param float $capacity
	 */
	public function setCapacity($capacity)
	{
		$this->capacity = $capacity;
	}

	/**
	 * @return float
	 */
	public function getCapacity()
	{
		return $this->capacity;
	}

	/**
	 * @param History[] $history
	 */
	public function setHistory($history)
	{
		$this->history = $history;
	}

	/**
	 * @return History[]
	 */
	public function getHistory()
	{
		return $this->history;
	}


	/**
	 * @param float $chanceOfDuplicating
	 */
	public function setChanceOfDuplicating($chanceOfDuplicating)
	{
		$this->chanceOfDuplicating = $chanceOfDuplicating;
	}

	/**
	 * @return float
	 */
	public function getChanceOfDuplicating()
	{
		return $this->chanceOfDuplicating;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param int $groupId
	 */
	public function setGroupId($groupId)
	{
		$this->groupId = $groupId;
	}

	/**
	 * @return int
	 */
	public function getGroupId()
	{
		return $this->groupId;
	}

	/**
	 * @param int $marketGroupId
	 */
	public function setMarketGroupId($marketGroupId)
	{
		$this->marketGroupId = $marketGroupId;
	}

	/**
	 * @return int
	 */
	public function getMarketGroupId()
	{
		return $this->marketGroupId;
	}

	/**
	 * @param float $mass
	 */
	public function setMass($mass)
	{
		$this->mass = $mass;
	}

	/**
	 * @return float
	 */
	public function getMass()
	{
		return $this->mass;
	}

	/**
	 * @param int $portionSize
	 */
	public function setPortionSize($portionSize)
	{
		$this->portionSize = $portionSize;
	}

	/**
	 * @return int
	 */
	public function getPortionSize()
	{
		return $this->portionSize;
	}

	/**
	 * @param int $published
	 */
	public function setPublished($published)
	{
		$this->published = $published;
	}

	/**
	 * @return int
	 */
	public function getPublished()
	{
		return $this->published;
	}

	/**
	 * @param int $raceId
	 */
	public function setRaceId($raceId)
	{
		$this->raceId = $raceId;
	}

	/**
	 * @return int
	 */
	public function getRaceId()
	{
		return $this->raceId;
	}

	/**
	 * @param int $typeId
	 */
	public function setTypeId($typeId)
	{
		$this->typeId = $typeId;
	}

	/**
	 * @return int
	 */
	public function getTypeId()
	{
		return $this->typeId;
	}

	/**
	 * @param string $typeName
	 */
	public function setTypeName($typeName)
	{
		$this->typeName = $typeName;
	}

	/**
	 * @return string
	 */
	public function getTypeName()
	{
		return $this->typeName;
	}

	/**
	 * @param float $volume
	 */
	public function setVolume($volume)
	{
		$this->volume = $volume;
	}

	/**
	 * @return float
	 */
	public function getVolume()
	{
		return $this->volume;
	}


}

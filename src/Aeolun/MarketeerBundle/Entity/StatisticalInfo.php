<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * StatisticalInfo
 *
 * @ORM\Table(name="statistical_info")
 * @ORM\Entity
 */
class StatisticalInfo
{
	/**
	 * @var ItemType $type
	 *
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="ItemType", inversedBy="statistics")
	 * @ORM\JoinColumn(name="type_id", referencedColumnName="type_id", onDelete="CASCADE")
	 */
	private $type;

	/**
	 * @var MapRegion $region
	 *
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="MapRegion", inversedBy="statistics")
	 * @ORM\JoinColumn(name="region_id", referencedColumnName="region_id", onDelete="CASCADE")
	 */
	private $region;

    /**
     * @var integer
     *
     * @ORM\Column(name="buy_available", type="bigint", nullable=true)
     */
    private $buyAvailable;

    /**
     * @var integer
     *
     * @ORM\Column(name="buy_count", type="bigint", nullable=true)
     */
    private $buyCount;

    /**
     * @var float
     *
     * @ORM\Column(name="buy_min", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $buyMin;

    /**
     * @var float
     *
     * @ORM\Column(name="buy_max", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $buyMax;

    /**
     * @var float
     *
     * @ORM\Column(name="buy_average", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $buyAverage;

    /**
     * @var float
     *
     * @ORM\Column(name="buy_mean", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $buyMean;

    /**
     * @var float
     *
     * @ORM\Column(name="buy_percentile90", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $buyPercentile90;

    /**
     * @var float
     *
     * @ORM\Column(name="buy_percentile10", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $buyPercentile10;

    /**
     * @var integer
     *
     * @ORM\Column(name="sell_available", type="bigint", nullable=true)
     */
    private $sellAvailable;

    /**
     * @var integer
     *
     * @ORM\Column(name="sell_count", type="bigint", nullable=true)
     */
    private $sellCount;

    /**
     * @var float
     *
     * @ORM\Column(name="sell_min", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $sellMin;

    /**
     * @var float
     *
     * @ORM\Column(name="sell_max", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $sellMax;

    /**
     * @var float
     *
     * @ORM\Column(name="sell_average", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $sellAverage;

    /**
     * @var float
     *
     * @ORM\Column(name="sell_mean", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $sellMean;

    /**
     * @var float
     *
     * @ORM\Column(name="sell_percentile90", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $sellPercentile90;

    /**
     * @var float
     *
     * @ORM\Column(name="sell_percentile10", type="decimal", precision=20, scale=2, nullable=true)
     */
    private $sellPercentile10;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    private $updated;

	/**
	 * @param int $buyAvailable
	 */
	public function setBuyAvailable($buyAvailable)
	{
		$this->buyAvailable = $buyAvailable;
	}

	/**
	 * @return int
	 */
	public function getBuyAvailable()
	{
		return $this->buyAvailable;
	}

	/**
	 * @param float $buyAverage
	 */
	public function setBuyAverage($buyAverage)
	{
		$this->buyAverage = $buyAverage;
	}

	/**
	 * @return float
	 */
	public function getBuyAverage()
	{
		return $this->buyAverage;
	}

	/**
	 * @param int $buyCount
	 */
	public function setBuyCount($buyCount)
	{
		$this->buyCount = $buyCount;
	}

	/**
	 * @return int
	 */
	public function getBuyCount()
	{
		return $this->buyCount;
	}

	/**
	 * @param float $buyMax
	 */
	public function setBuyMax($buyMax)
	{
		$this->buyMax = $buyMax;
	}

	/**
	 * @return float
	 */
	public function getBuyMax()
	{
		return $this->buyMax;
	}

	/**
	 * @param float $buyMean
	 */
	public function setBuyMean($buyMean)
	{
		$this->buyMean = $buyMean;
	}

	/**
	 * @return float
	 */
	public function getBuyMean()
	{
		return $this->buyMean;
	}

	/**
	 * @param float $buyMin
	 */
	public function setBuyMin($buyMin)
	{
		$this->buyMin = $buyMin;
	}

	/**
	 * @return float
	 */
	public function getBuyMin()
	{
		return $this->buyMin;
	}

	/**
	 * @param float $buyPercentile10
	 */
	public function setBuyPercentile10($buyPercentile10)
	{
		$this->buyPercentile10 = $buyPercentile10;
	}

	/**
	 * @return float
	 */
	public function getBuyPercentile10()
	{
		return $this->buyPercentile10;
	}

	/**
	 * @param float $buyPercentile90
	 */
	public function setBuyPercentile90($buyPercentile90)
	{
		$this->buyPercentile90 = $buyPercentile90;
	}

	/**
	 * @return float
	 */
	public function getBuyPercentile90()
	{
		return $this->buyPercentile90;
	}

	/**
	 * @param \Aeolun\MarketeerBundle\Entity\MapRegion $region
	 */
	public function setRegion($region)
	{
		$this->region = $region;
	}

	/**
	 * @return \Aeolun\MarketeerBundle\Entity\MapRegion
	 */
	public function getRegion()
	{
		return $this->region;
	}

	/**
	 * @param int $sellAvailable
	 */
	public function setSellAvailable($sellAvailable)
	{
		$this->sellAvailable = $sellAvailable;
	}

	/**
	 * @return int
	 */
	public function getSellAvailable()
	{
		return $this->sellAvailable;
	}

	/**
	 * @param float $sellAverage
	 */
	public function setSellAverage($sellAverage)
	{
		$this->sellAverage = $sellAverage;
	}

	/**
	 * @return float
	 */
	public function getSellAverage()
	{
		return $this->sellAverage;
	}

	/**
	 * @param int $sellCount
	 */
	public function setSellCount($sellCount)
	{
		$this->sellCount = $sellCount;
	}

	/**
	 * @return int
	 */
	public function getSellCount()
	{
		return $this->sellCount;
	}

	/**
	 * @param float $sellMax
	 */
	public function setSellMax($sellMax)
	{
		$this->sellMax = $sellMax;
	}

	/**
	 * @return float
	 */
	public function getSellMax()
	{
		return $this->sellMax;
	}

	/**
	 * @param float $sellMean
	 */
	public function setSellMean($sellMean)
	{
		$this->sellMean = $sellMean;
	}

	/**
	 * @return float
	 */
	public function getSellMean()
	{
		return $this->sellMean;
	}

	/**
	 * @param float $sellMin
	 */
	public function setSellMin($sellMin)
	{
		$this->sellMin = $sellMin;
	}

	/**
	 * @return float
	 */
	public function getSellMin()
	{
		return $this->sellMin;
	}

	/**
	 * @param float $sellPercentile10
	 */
	public function setSellPercentile10($sellPercentile10)
	{
		$this->sellPercentile10 = $sellPercentile10;
	}

	/**
	 * @return float
	 */
	public function getSellPercentile10()
	{
		return $this->sellPercentile10;
	}

	/**
	 * @param float $sellPercentile90
	 */
	public function setSellPercentile90($sellPercentile90)
	{
		$this->sellPercentile90 = $sellPercentile90;
	}

	/**
	 * @return float
	 */
	public function getSellPercentile90()
	{
		return $this->sellPercentile90;
	}

	/**
	 * @param \DateTime $updated
	 */
	public function setUpdated($updated)
	{
		$this->updated = $updated;
	}

	/**
	 * @return \DateTime
	 */
	public function getUpdated()
	{
		return $this->updated;
	}

	/**
	 * @param ItemType $type
	 */
	public function setType(ItemType $type)
	{
		$this->type = $type;
	}

	/**
	 * @return ItemType
	 */
	public function getType()
	{
		return $this->type;
	}

}

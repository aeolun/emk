<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * DogmaEffects
 *
 * @ORM\Table(name="dogma_effects")
 * @ORM\Entity
 */
class DogmaEffects
{
    /**
     * @var integer
     *
     * @ORM\Column(name="effect_id", type="smallint", nullable=false)
     * @ORM\Id

     */
    private $effectId;

    /**
     * @var string
     *
     * @ORM\Column(name="effect_name", type="string", length=400, nullable=true)
     */
    private $effectName;

    /**
     * @var integer
     *
     * @ORM\Column(name="effect_category", type="smallint", nullable=true)
     */
    private $effectCategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="pre_expression", type="integer", nullable=true)
     */
    private $preExpression;

    /**
     * @var integer
     *
     * @ORM\Column(name="post_expression", type="integer", nullable=true)
     */
    private $postExpression;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="string", length=60, nullable=true)
     */
    private $guid;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_offensive", type="integer", nullable=true)
     */
    private $isOffensive;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_assistance", type="integer", nullable=true)
     */
    private $isAssistance;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration_attribute_id", type="smallint", nullable=true)
     */
    private $durationAttributeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tracking_speed_attribute_id", type="smallint", nullable=true)
     */
    private $trackingSpeedAttributeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="discharge_attribute_id", type="smallint", nullable=true)
     */
    private $dischargeAttributeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="range_attribute_id", type="smallint", nullable=true)
     */
    private $rangeAttributeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="falloff_attribute_id", type="smallint", nullable=true)
     */
    private $falloffAttributeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="disallow_auto_repeat", type="integer", nullable=true)
     */
    private $disallowAutoRepeat;

    /**
     * @var integer
     *
     * @ORM\Column(name="published", type="integer", nullable=true)
     */
    private $published;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=100, nullable=true)
     */
    private $displayName;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_warp_safe", type="integer", nullable=true)
     */
    private $isWarpSafe;

    /**
     * @var integer
     *
     * @ORM\Column(name="range_chance", type="integer", nullable=true)
     */
    private $rangeChance;

    /**
     * @var integer
     *
     * @ORM\Column(name="electronic_chance", type="integer", nullable=true)
     */
    private $electronicChance;

    /**
     * @var integer
     *
     * @ORM\Column(name="propulsion_chance", type="integer", nullable=true)
     */
    private $propulsionChance;

    /**
     * @var integer
     *
     * @ORM\Column(name="distribution", type="integer", nullable=true)
     */
    private $distribution;

    /**
     * @var string
     *
     * @ORM\Column(name="sfx_name", type="string", length=20, nullable=true)
     */
    private $sfxName;

    /**
     * @var integer
     *
     * @ORM\Column(name="npc_usage_chance_attribute_id", type="smallint", nullable=true)
     */
    private $npcUsageChanceAttributeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="npc_activation_chance_attribute_id", type="smallint", nullable=true)
     */
    private $npcActivationChanceAttributeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="fitting_usage_chance_attribute_id", type="smallint", nullable=true)
     */
    private $fittingUsageChanceAttributeId;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * CertificateClasses
 *
 * @ORM\Table(name="certificate_classes")
 * @ORM\Entity
 */
class CertificateClasses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="class_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $classId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="class_name", type="string", length=512, nullable=true)
     */
    private $className;


}

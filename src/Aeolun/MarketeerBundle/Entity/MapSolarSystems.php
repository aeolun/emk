<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapSolarSystems
 *
 * @ORM\Table(name="map_solar_systems", indexes={@Index(name="map_solar_systems_IX_region", columns={"region_id"}), @Index(name="map_solar_systems_IX_constellation", columns={"constellation_id"}), @Index(name="map_solar_systems_IX_security", columns={"security"})})
 * @ORM\Entity
 */
class MapSolarSystems
{
    /**
     * @var integer
     *
     * @ORM\Column(name="solar_system_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $solarSystemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="region_id", type="integer", nullable=true)
     */
    private $regionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="constellation_id", type="integer", nullable=true)
     */
    private $constellationId;

    /**
     * @var string
     *
     * @ORM\Column(name="solar_system_name", type="string", length=200, nullable=true)
     */
    private $solarSystemName;

    /**
     * @var float
     *
     * @ORM\Column(name="x", type="float", nullable=true)
     */
    private $x;

    /**
     * @var float
     *
     * @ORM\Column(name="y", type="float", nullable=true)
     */
    private $y;

    /**
     * @var float
     *
     * @ORM\Column(name="z", type="float", nullable=true)
     */
    private $z;

    /**
     * @var float
     *
     * @ORM\Column(name="x_min", type="float", nullable=true)
     */
    private $xMin;

    /**
     * @var float
     *
     * @ORM\Column(name="x_max", type="float", nullable=true)
     */
    private $xMax;

    /**
     * @var float
     *
     * @ORM\Column(name="y_min", type="float", nullable=true)
     */
    private $yMin;

    /**
     * @var float
     *
     * @ORM\Column(name="y_max", type="float", nullable=true)
     */
    private $yMax;

    /**
     * @var float
     *
     * @ORM\Column(name="z_min", type="float", nullable=true)
     */
    private $zMin;

    /**
     * @var float
     *
     * @ORM\Column(name="z_max", type="float", nullable=true)
     */
    private $zMax;

    /**
     * @var float
     *
     * @ORM\Column(name="luminosity", type="float", nullable=true)
     */
    private $luminosity;

    /**
     * @var integer
     *
     * @ORM\Column(name="border", type="integer", nullable=true)
     */
    private $border;

    /**
     * @var integer
     *
     * @ORM\Column(name="fringe", type="integer", nullable=true)
     */
    private $fringe;

    /**
     * @var integer
     *
     * @ORM\Column(name="corridor", type="integer", nullable=true)
     */
    private $corridor;

    /**
     * @var integer
     *
     * @ORM\Column(name="hub", type="integer", nullable=true)
     */
    private $hub;

    /**
     * @var integer
     *
     * @ORM\Column(name="international", type="integer", nullable=true)
     */
    private $international;

    /**
     * @var integer
     *
     * @ORM\Column(name="regional", type="integer", nullable=true)
     */
    private $regional;

    /**
     * @var integer
     *
     * @ORM\Column(name="constellation", type="integer", nullable=true)
     */
    private $constellation;

    /**
     * @var float
     *
     * @ORM\Column(name="security", type="float", nullable=true)
     */
    private $security;

    /**
     * @var integer
     *
     * @ORM\Column(name="faction_id", type="integer", nullable=true)
     */
    private $factionId;

    /**
     * @var float
     *
     * @ORM\Column(name="radius", type="float", nullable=true)
     */
    private $radius;

    /**
     * @var integer
     *
     * @ORM\Column(name="sun_type_id", type="integer", nullable=true)
     */
    private $sunTypeId;

    /**
     * @var string
     *
     * @ORM\Column(name="security_class", type="string", length=2, nullable=true)
     */
    private $securityClass;

	/**
	 * @param int $border
	 */
	public function setBorder($border)
	{
		$this->border = $border;
	}

	/**
	 * @return int
	 */
	public function getBorder()
	{
		return $this->border;
	}

	/**
	 * @param int $constellation
	 */
	public function setConstellation($constellation)
	{
		$this->constellation = $constellation;
	}

	/**
	 * @return int
	 */
	public function getConstellation()
	{
		return $this->constellation;
	}

	/**
	 * @param int $constellationId
	 */
	public function setConstellationId($constellationId)
	{
		$this->constellationId = $constellationId;
	}

	/**
	 * @return int
	 */
	public function getConstellationId()
	{
		return $this->constellationId;
	}

	/**
	 * @param int $corridor
	 */
	public function setCorridor($corridor)
	{
		$this->corridor = $corridor;
	}

	/**
	 * @return int
	 */
	public function getCorridor()
	{
		return $this->corridor;
	}

	/**
	 * @param int $factionId
	 */
	public function setFactionId($factionId)
	{
		$this->factionId = $factionId;
	}

	/**
	 * @return int
	 */
	public function getFactionId()
	{
		return $this->factionId;
	}

	/**
	 * @param int $fringe
	 */
	public function setFringe($fringe)
	{
		$this->fringe = $fringe;
	}

	/**
	 * @return int
	 */
	public function getFringe()
	{
		return $this->fringe;
	}

	/**
	 * @param int $hub
	 */
	public function setHub($hub)
	{
		$this->hub = $hub;
	}

	/**
	 * @return int
	 */
	public function getHub()
	{
		return $this->hub;
	}

	/**
	 * @param int $international
	 */
	public function setInternational($international)
	{
		$this->international = $international;
	}

	/**
	 * @return int
	 */
	public function getInternational()
	{
		return $this->international;
	}

	/**
	 * @param float $luminosity
	 */
	public function setLuminosity($luminosity)
	{
		$this->luminosity = $luminosity;
	}

	/**
	 * @return float
	 */
	public function getLuminosity()
	{
		return $this->luminosity;
	}

	/**
	 * @param float $radius
	 */
	public function setRadius($radius)
	{
		$this->radius = $radius;
	}

	/**
	 * @return float
	 */
	public function getRadius()
	{
		return $this->radius;
	}

	/**
	 * @param int $regionId
	 */
	public function setRegionId($regionId)
	{
		$this->regionId = $regionId;
	}

	/**
	 * @return int
	 */
	public function getRegionId()
	{
		return $this->regionId;
	}

	/**
	 * @param int $regional
	 */
	public function setRegional($regional)
	{
		$this->regional = $regional;
	}

	/**
	 * @return int
	 */
	public function getRegional()
	{
		return $this->regional;
	}

	/**
	 * @param float $security
	 */
	public function setSecurity($security)
	{
		$this->security = $security;
	}

	/**
	 * @return float
	 */
	public function getSecurity()
	{
		return $this->security;
	}

	/**
	 * @param string $securityClass
	 */
	public function setSecurityClass($securityClass)
	{
		$this->securityClass = $securityClass;
	}

	/**
	 * @return string
	 */
	public function getSecurityClass()
	{
		return $this->securityClass;
	}

	/**
	 * @param int $solarSystemId
	 */
	public function setSolarSystemId($solarSystemId)
	{
		$this->solarSystemId = $solarSystemId;
	}

	/**
	 * @return int
	 */
	public function getSolarSystemId()
	{
		return $this->solarSystemId;
	}

	/**
	 * @param string $solarSystemName
	 */
	public function setSolarSystemName($solarSystemName)
	{
		$this->solarSystemName = $solarSystemName;
	}

	/**
	 * @return string
	 */
	public function getSolarSystemName()
	{
		return $this->solarSystemName;
	}

	/**
	 * @param int $sunTypeId
	 */
	public function setSunTypeId($sunTypeId)
	{
		$this->sunTypeId = $sunTypeId;
	}

	/**
	 * @return int
	 */
	public function getSunTypeId()
	{
		return $this->sunTypeId;
	}

	/**
	 * @param float $x
	 */
	public function setX($x)
	{
		$this->x = $x;
	}

	/**
	 * @return float
	 */
	public function getX()
	{
		return $this->x;
	}

	/**
	 * @param float $xMax
	 */
	public function setXMax($xMax)
	{
		$this->xMax = $xMax;
	}

	/**
	 * @return float
	 */
	public function getXMax()
	{
		return $this->xMax;
	}

	/**
	 * @param float $xMin
	 */
	public function setXMin($xMin)
	{
		$this->xMin = $xMin;
	}

	/**
	 * @return float
	 */
	public function getXMin()
	{
		return $this->xMin;
	}

	/**
	 * @param float $y
	 */
	public function setY($y)
	{
		$this->y = $y;
	}

	/**
	 * @return float
	 */
	public function getY()
	{
		return $this->y;
	}

	/**
	 * @param float $yMax
	 */
	public function setYMax($yMax)
	{
		$this->yMax = $yMax;
	}

	/**
	 * @return float
	 */
	public function getYMax()
	{
		return $this->yMax;
	}

	/**
	 * @param float $yMin
	 */
	public function setYMin($yMin)
	{
		$this->yMin = $yMin;
	}

	/**
	 * @return float
	 */
	public function getYMin()
	{
		return $this->yMin;
	}

	/**
	 * @param float $z
	 */
	public function setZ($z)
	{
		$this->z = $z;
	}

	/**
	 * @return float
	 */
	public function getZ()
	{
		return $this->z;
	}

	/**
	 * @param float $zMax
	 */
	public function setZMax($zMax)
	{
		$this->zMax = $zMax;
	}

	/**
	 * @return float
	 */
	public function getZMax()
	{
		return $this->zMax;
	}

	/**
	 * @param float $zMin
	 */
	public function setZMin($zMin)
	{
		$this->zMin = $zMin;
	}

	/**
	 * @return float
	 */
	public function getZMin()
	{
		return $this->zMin;
	}


}

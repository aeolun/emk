<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * IndustrialAssemblyLines
 *
 * @ORM\Table(name="industrial_assembly_lines", indexes={@Index(name="industrial_assembly_lines_IX_container", columns={"container_id"}), @Index(name="industrial_assembly_lines_IX_owner", columns={"owner_id"})})
 * @ORM\Entity
 */
class IndustrialAssemblyLines
{
    /**
     * @var integer
     *
     * @ORM\Column(name="assembly_line_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $assemblyLineId;

    /**
     * @var integer
     *
     * @ORM\Column(name="assembly_line_type_id", type="integer", nullable=true)
     */
    private $assemblyLineTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="container_id", type="integer", nullable=true)
     */
    private $containerId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next_free_time", type="datetime", nullable=true)
     */
    private $nextFreeTime;

    /**
     * @var integer
     *
     * @ORM\Column(name="UIGrouping_id", type="integer", nullable=true)
     */
    private $uigroupingId;

    /**
     * @var float
     *
     * @ORM\Column(name="cost_install", type="float", nullable=true)
     */
    private $costInstall;

    /**
     * @var float
     *
     * @ORM\Column(name="cost_per_hour", type="float", nullable=true)
     */
    private $costPerHour;

    /**
     * @var integer
     *
     * @ORM\Column(name="restriction_mask", type="integer", nullable=true)
     */
    private $restrictionMask;

    /**
     * @var float
     *
     * @ORM\Column(name="discount_per_good_standing_point", type="float", nullable=true)
     */
    private $discountPerGoodStandingPoint;

    /**
     * @var float
     *
     * @ORM\Column(name="surcharge_per_bad_standing_point", type="float", nullable=true)
     */
    private $surchargePerBadStandingPoint;

    /**
     * @var float
     *
     * @ORM\Column(name="minimum_standing", type="float", nullable=true)
     */
    private $minimumStanding;

    /**
     * @var float
     *
     * @ORM\Column(name="minimum_char_security", type="float", nullable=true)
     */
    private $minimumCharSecurity;

    /**
     * @var float
     *
     * @ORM\Column(name="minimum_corp_security", type="float", nullable=true)
     */
    private $minimumCorpSecurity;

    /**
     * @var float
     *
     * @ORM\Column(name="maximum_char_security", type="float", nullable=true)
     */
    private $maximumCharSecurity;

    /**
     * @var float
     *
     * @ORM\Column(name="maximum_corp_security", type="float", nullable=true)
     */
    private $maximumCorpSecurity;

    /**
     * @var integer
     *
     * @ORM\Column(name="owner_id", type="integer", nullable=true)
     */
    private $ownerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="integer", nullable=true)
     */
    private $activityId;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * IndustrialAssemblyLineTypeDetailPerCategory
 *
 * @ORM\Table(name="industrial_assembly_line_type_detail_per_category")
 * @ORM\Entity
 */
class IndustrialAssemblyLineTypeDetailPerCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="assembly_line_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $assemblyLineTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $categoryId;

    /**
     * @var float
     *
     * @ORM\Column(name="time_multiplier", type="float", nullable=true)
     */
    private $timeMultiplier;

    /**
     * @var float
     *
     * @ORM\Column(name="material_multiplier", type="float", nullable=true)
     */
    private $materialMultiplier;


}

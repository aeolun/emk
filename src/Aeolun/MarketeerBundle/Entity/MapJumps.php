<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapJumps
 *
 * @ORM\Table(name="map_jumps")
 * @ORM\Entity
 */
class MapJumps
{
    /**
     * @var integer
     *
     * @ORM\Column(name="stargate_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $stargateId;

    /**
     * @var integer
     *
     * @ORM\Column(name="celestial_id", type="integer", nullable=true)
     */
    private $celestialId;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * TranslationTables
 *
 * @ORM\Table(name="translation_tables")
 * @ORM\Entity
 */
class TranslationTables
{
    /**
     * @var string
     *
     * @ORM\Column(name="source_table", type="string", length=400, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $sourceTable;

    /**
     * @var string
     *
     * @ORM\Column(name="translated_key", type="string", length=400, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $translatedKey;

    /**
     * @var string
     *
     * @ORM\Column(name="destination_table", type="string", length=400, nullable=true)
     */
    private $destinationTable;

    /**
     * @var integer
     *
     * @ORM\Column(name="tc_group_id", type="integer", nullable=true)
     */
    private $tcGroupId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tc_id", type="integer", nullable=true)
     */
    private $tcId;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * CertificateCategories
 *
 * @ORM\Table(name="certificate_categories")
 * @ORM\Entity
 */
class CertificateCategories
{
    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $categoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="category_name", type="string", length=512, nullable=true)
     */
    private $categoryName;


}

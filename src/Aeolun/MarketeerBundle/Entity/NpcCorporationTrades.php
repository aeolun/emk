<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * NpcCorporationTrades
 *
 * @ORM\Table(name="npc_corporation_trades")
 * @ORM\Entity
 */
class NpcCorporationTrades
{
    /**
     * @var integer
     *
     * @ORM\Column(name="corporation_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $corporationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $typeId;


}

<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * HistoricalSummary
 *
 * @ORM\Table(name="historical_summary")
 * @ORM\Entity
 */
class HistoricalSummary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="region_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $regionId;

    /**
     * @var float
     *
     * @ORM\Column(name="price_60d", type="float", nullable=false)
     */
    private $price60d;

    /**
     * @var float
     *
     * @ORM\Column(name="price_20d", type="float", nullable=false)
     */
    private $price20d;

    /**
     * @var float
     *
     * @ORM\Column(name="price_5d", type="float", nullable=false)
     */
    private $price5d;

    /**
     * @var float
     *
     * @ORM\Column(name="high_60d", type="float", nullable=false)
     */
    private $high60d;

    /**
     * @var float
     *
     * @ORM\Column(name="high_20d", type="float", nullable=false)
     */
    private $high20d;

    /**
     * @var float
     *
     * @ORM\Column(name="high_5d", type="float", nullable=false)
     */
    private $high5d;

    /**
     * @var float
     *
     * @ORM\Column(name="low_60d", type="float", nullable=false)
     */
    private $low60d;

    /**
     * @var float
     *
     * @ORM\Column(name="low_20d", type="float", nullable=false)
     */
    private $low20d;

    /**
     * @var float
     *
     * @ORM\Column(name="low_5d", type="float", nullable=false)
     */
    private $low5d;

    /**
     * @var float
     *
     * @ORM\Column(name="bought_60d", type="float", nullable=false)
     */
    private $bought60d;

    /**
     * @var float
     *
     * @ORM\Column(name="bought_20d", type="float", nullable=false)
     */
    private $bought20d;

    /**
     * @var float
     *
     * @ORM\Column(name="bought_5d", type="float", nullable=false)
     */
    private $bought5d;

    /**
     * @var float
     *
     * @ORM\Column(name="sold_60d", type="float", nullable=false)
     */
    private $sold60d;

    /**
     * @var float
     *
     * @ORM\Column(name="sold_20d", type="float", nullable=false)
     */
    private $sold20d;

    /**
     * @var float
     *
     * @ORM\Column(name="sold_5d", type="float", nullable=false)
     */
    private $sold5d;

    /**
     * @var float
     *
     * @ORM\Column(name="orders_60d", type="float", nullable=false)
     */
    private $orders60d;

    /**
     * @var float
     *
     * @ORM\Column(name="orders_20d", type="float", nullable=false)
     */
    private $orders20d;

    /**
     * @var float
     *
     * @ORM\Column(name="orders_5d", type="float", nullable=false)
     */
    private $orders5d;

    /**
     * @var float
     *
     * @ORM\Column(name="transactions_60d", type="float", nullable=false)
     */
    private $transactions60d;

    /**
     * @var float
     *
     * @ORM\Column(name="transactions_20d", type="float", nullable=false)
     */
    private $transactions20d;

    /**
     * @var float
     *
     * @ORM\Column(name="transactions_5d", type="float", nullable=false)
     */
    private $transactions5d;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    private $updated;


}

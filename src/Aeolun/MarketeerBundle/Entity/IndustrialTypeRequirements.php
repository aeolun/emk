<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * IndustrialTypeRequirements
 *
 * @ORM\Table(name="industrial_type_requirements")
 * @ORM\Entity
 */
class IndustrialTypeRequirements
{
    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $typeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $activityId;

    /**
     * @var integer
     *
     * @ORM\Column(name="required_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $requiredTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="damage_per_job", type="float", nullable=true)
     */
    private $damagePerJob;

    /**
     * @var integer
     *
     * @ORM\Column(name="recycle", type="integer", nullable=true)
     */
    private $recycle;


}

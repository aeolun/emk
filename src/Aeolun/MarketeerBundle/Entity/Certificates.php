<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * Certificates
 *
 * @ORM\Table(name="certificates", indexes={@Index(name="certificates_IX_category", columns={"category_id"}), @Index(name="certificates_IX_class", columns={"class_id"})})
 * @ORM\Entity
 */
class Certificates
{
    /**
     * @var integer
     *
     * @ORM\Column(name="certificate_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $certificateId;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @ORM\Column(name="class_id", type="integer", nullable=true)
     */
    private $classId;

    /**
     * @var integer
     *
     * @ORM\Column(name="grade", type="integer", nullable=true)
     */
    private $grade;

    /**
     * @var integer
     *
     * @ORM\Column(name="corp_id", type="integer", nullable=true)
     */
    private $corpId;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;


}

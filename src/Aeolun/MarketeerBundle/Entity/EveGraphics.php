<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * EveGraphics
 *
 * @ORM\Table(name="eve_graphics")
 * @ORM\Entity
 */
class EveGraphics
{
    /**
     * @var integer
     *
     * @ORM\Column(name="graphic_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $graphicId;

    /**
     * @var string
     *
     * @ORM\Column(name="graphic_file", type="string", length=500, nullable=false)
     */
    private $graphicFile;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="obsolete", type="integer", nullable=false)
     */
    private $obsolete;

    /**
     * @var string
     *
     * @ORM\Column(name="graphic_type", type="string", length=100, nullable=true)
     */
    private $graphicType;

    /**
     * @var integer
     *
     * @ORM\Column(name="collidable", type="integer", nullable=true)
     */
    private $collidable;

    /**
     * @var integer
     *
     * @ORM\Column(name="explosion_id", type="integer", nullable=true)
     */
    private $explosionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="directory_id", type="integer", nullable=true)
     */
    private $directoryId;

    /**
     * @var string
     *
     * @ORM\Column(name="graphic_name", type="string", length=64, nullable=false)
     */
    private $graphicName;


}

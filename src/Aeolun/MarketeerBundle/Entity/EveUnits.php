<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * EveUnits
 *
 * @ORM\Table(name="eve_units")
 * @ORM\Entity
 */
class EveUnits
{
    /**
     * @var integer
     *
     * @ORM\Column(name="unit_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $unitId;

    /**
     * @var string
     *
     * @ORM\Column(name="unit_name", type="string", length=100, nullable=true)
     */
    private $unitName;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=50, nullable=true)
     */
    private $displayName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;


}

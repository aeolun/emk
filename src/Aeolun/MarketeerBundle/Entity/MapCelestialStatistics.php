<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapCelestialStatistics
 *
 * @ORM\Table(name="map_celestial_statistics")
 * @ORM\Entity
 */
class MapCelestialStatistics
{
    /**
     * @var integer
     *
     * @ORM\Column(name="celestial_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $celestialId;

    /**
     * @var float
     *
     * @ORM\Column(name="temperature", type="float", nullable=true)
     */
    private $temperature;

    /**
     * @var string
     *
     * @ORM\Column(name="spectral_class", type="string", length=10, nullable=true)
     */
    private $spectralClass;

    /**
     * @var float
     *
     * @ORM\Column(name="luminosity", type="float", nullable=true)
     */
    private $luminosity;

    /**
     * @var float
     *
     * @ORM\Column(name="age", type="float", nullable=true)
     */
    private $age;

    /**
     * @var float
     *
     * @ORM\Column(name="life", type="float", nullable=true)
     */
    private $life;

    /**
     * @var float
     *
     * @ORM\Column(name="orbit_radius", type="float", nullable=true)
     */
    private $orbitRadius;

    /**
     * @var float
     *
     * @ORM\Column(name="eccentricity", type="float", nullable=true)
     */
    private $eccentricity;

    /**
     * @var float
     *
     * @ORM\Column(name="mass_dust", type="float", nullable=true)
     */
    private $massDust;

    /**
     * @var float
     *
     * @ORM\Column(name="mass_gas", type="float", nullable=true)
     */
    private $massGas;

    /**
     * @var integer
     *
     * @ORM\Column(name="fragmented", type="integer", nullable=true)
     */
    private $fragmented;

    /**
     * @var float
     *
     * @ORM\Column(name="density", type="float", nullable=true)
     */
    private $density;

    /**
     * @var float
     *
     * @ORM\Column(name="surface_gravity", type="float", nullable=true)
     */
    private $surfaceGravity;

    /**
     * @var float
     *
     * @ORM\Column(name="escape_velocity", type="float", nullable=true)
     */
    private $escapeVelocity;

    /**
     * @var float
     *
     * @ORM\Column(name="orbit_period", type="float", nullable=true)
     */
    private $orbitPeriod;

    /**
     * @var float
     *
     * @ORM\Column(name="rotation_rate", type="float", nullable=true)
     */
    private $rotationRate;

    /**
     * @var integer
     *
     * @ORM\Column(name="locked", type="integer", nullable=true)
     */
    private $locked;

    /**
     * @var float
     *
     * @ORM\Column(name="pressure", type="float", nullable=true)
     */
    private $pressure;

    /**
     * @var float
     *
     * @ORM\Column(name="radius", type="float", nullable=true)
     */
    private $radius;

    /**
     * @var float
     *
     * @ORM\Column(name="mass", type="float", nullable=true)
     */
    private $mass;


}

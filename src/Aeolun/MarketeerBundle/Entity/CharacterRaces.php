<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * CharacterRaces
 *
 * @ORM\Table(name="character_races")
 * @ORM\Entity
 */
class CharacterRaces
{
    /**
     * @var integer
     *
     * @ORM\Column(name="race_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $raceId;

    /**
     * @var string
     *
     * @ORM\Column(name="race_name", type="string", length=100, nullable=true)
     */
    private $raceName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;

    /**
     * @var string
     *
     * @ORM\Column(name="short_description", type="string", length=500, nullable=true)
     */
    private $shortDescription;


}

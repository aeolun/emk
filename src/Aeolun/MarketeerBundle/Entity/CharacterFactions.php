<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * CharacterFactions
 *
 * @ORM\Table(name="character_factions")
 * @ORM\Entity
 */
class CharacterFactions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="faction_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $factionId;

    /**
     * @var string
     *
     * @ORM\Column(name="faction_name", type="string", length=100, nullable=true)
     */
    private $factionName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="race_ids", type="integer", nullable=true)
     */
    private $raceIds;

    /**
     * @var integer
     *
     * @ORM\Column(name="solar_system_id", type="integer", nullable=true)
     */
    private $solarSystemId;

    /**
     * @var integer
     *
     * @ORM\Column(name="corporation_id", type="integer", nullable=true)
     */
    private $corporationId;

    /**
     * @var float
     *
     * @ORM\Column(name="size_factor", type="float", nullable=true)
     */
    private $sizeFactor;

    /**
     * @var integer
     *
     * @ORM\Column(name="station_count", type="smallint", nullable=true)
     */
    private $stationCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="station_system_count", type="smallint", nullable=true)
     */
    private $stationSystemCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="militia_corporation_id", type="integer", nullable=true)
     */
    private $militiaCorporationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;


}

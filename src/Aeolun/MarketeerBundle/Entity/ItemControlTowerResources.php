<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemControlTowerResources
 *
 * @ORM\Table(name="item_control_tower_resources")
 * @ORM\Entity
 */
class ItemControlTowerResources
{
    /**
     * @var integer
     *
     * @ORM\Column(name="control_tower_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $controlTowerTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="resource_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $resourceTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="purpose", type="integer", nullable=true)
     */
    private $purpose;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="min_security_level", type="float", nullable=true)
     */
    private $minSecurityLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="faction_id", type="integer", nullable=true)
     */
    private $factionId;


}

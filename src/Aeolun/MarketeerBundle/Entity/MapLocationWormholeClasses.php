<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * MapLocationWormholeClasses
 *
 * @ORM\Table(name="map_location_wormhole_classes")
 * @ORM\Entity
 */
class MapLocationWormholeClasses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="location_id", type="integer", nullable=false)
     * @ORM\Id

     */
    private $locationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="wormhole_class_id", type="integer", nullable=true)
     */
    private $wormholeClassId;


}

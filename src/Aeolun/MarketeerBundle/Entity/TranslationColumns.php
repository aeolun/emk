<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * TranslationColumns
 *
 * @ORM\Table(name="translation_columns")
 * @ORM\Entity
 */
class TranslationColumns
{
    /**
     * @var integer
     *
     * @ORM\Column(name="tc_id", type="smallint", nullable=false)
     * @ORM\Id

     */
    private $tcId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tc_group_id", type="smallint", nullable=true)
     */
    private $tcGroupId;

    /**
     * @var string
     *
     * @ORM\Column(name="table_name", type="string", length=512, nullable=false)
     */
    private $tableName;

    /**
     * @var string
     *
     * @ORM\Column(name="column_name", type="string", length=256, nullable=false)
     */
    private $columnName;

    /**
     * @var string
     *
     * @ORM\Column(name="master_id", type="string", length=256, nullable=true)
     */
    private $masterId;


}

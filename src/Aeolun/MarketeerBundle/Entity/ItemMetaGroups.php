<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * ItemMetaGroups
 *
 * @ORM\Table(name="item_meta_groups")
 * @ORM\Entity
 */
class ItemMetaGroups
{
    /**
     * @var integer
     *
     * @ORM\Column(name="meta_group_id", type="smallint", nullable=false)
     * @ORM\Id

     */
    private $metaGroupId;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_group_name", type="string", length=200, nullable=true)
     */
    private $metaGroupName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="icon_id", type="integer", nullable=true)
     */
    private $iconId;


}

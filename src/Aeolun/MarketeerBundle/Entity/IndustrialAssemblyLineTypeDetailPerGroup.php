<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * IndustrialAssemblyLineTypeDetailPerGroup
 *
 * @ORM\Table(name="industrial_assembly_line_type_detail_per_group")
 * @ORM\Entity
 */
class IndustrialAssemblyLineTypeDetailPerGroup
{
    /**
     * @var integer
     *
     * @ORM\Column(name="assembly_line_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $assemblyLineTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="group_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $groupId;

    /**
     * @var float
     *
     * @ORM\Column(name="time_multiplier", type="float", nullable=true)
     */
    private $timeMultiplier;

    /**
     * @var float
     *
     * @ORM\Column(name="material_multiplier", type="float", nullable=true)
     */
    private $materialMultiplier;


}

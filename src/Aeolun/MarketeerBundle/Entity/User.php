<?php

namespace Aeolun\MarketeerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index as Index;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity
 */
class User extends \FOS\UserBundle\Model\User
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

	/**
	 * @ORM\ManyToMany(targetEntity="MapRegion", inversedBy="users")
	 * @ORM\JoinTable(name="map_region_user", joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="map_region_id", referencedColumnName="region_id")})
	 */
	private $regions;

	/**
	 * @var MapRegion $primaryRegion
	 *
	 * @ORM\ManyToOne(targetEntity="MapRegion", inversedBy="primaryUsers")
	 * @ORM\JoinColumn(name="primary_region_id", referencedColumnName="region_id", onDelete="SET NULL")
	 */
	private $primaryRegion;

	function __construct() {
		$this->regions = new ArrayCollection();
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param ArrayCollection $regions
	 */
	public function setRegions($regions)
	{
		$this->regions = $regions;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getRegions()
	{
		return $this->regions;
	}

	public function addRegion(MapRegion $region) {
		$this->getRegions()->add($region);
	}

	public function removeRegion(MapRegion $region) {
		$this->getRegions()->remove($region);
	}

	public function clearRegions() {
		$this->getRegions()->clear();
	}

	/**
	 * @param \Aeolun\MarketeerBundle\Entity\MapRegion $primaryRegion
	 */
	public function setPrimaryRegion($primaryRegion)
	{
		$this->primaryRegion = $primaryRegion;
	}

	/**
	 * @return \Aeolun\MarketeerBundle\Entity\MapRegion
	 */
	public function getPrimaryRegion()
	{
		return $this->primaryRegion;
	}

}

<?php
/**
 * User: Bart
 * Date: 14-12-13
 * Time: 15:26
 * 
 */

namespace Aeolun\MarketeerBundle;


class DateTimeId extends \DateTime
{
	public function __toString()
	{
		return $this->format('U');
	}
}
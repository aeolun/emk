/**
 * Created by Bart on 28-11-13.
 */
$(document).ready(function() {
	$('ul ul').slideDown(0).slideUp(0);
	$(document).on('click', '.is-dropdown-menu', function(event) {
		event.preventDefault();
		$(this).parent().toggleClass('active');
		$(this).next().slideToggle('fast');
	})

	$('#search').on('change', function() {
		window.location.hash = $(this).val();
		$.ajax(Routing.generate('search'), {
			data: {
				q: $(this).val()
			},
			success: function(data) {
				$('#searchResults').html(data);
				$('#searchResults').show();
				$('#content').hide();
			}
		});
	})
	if (window.location.hash != '') {
		$('#search').val(window.location.hash.substr(1));
		$.ajax(Routing.generate('search'), {
			data: {
				q: window.location.hash.substr(1)
			},
			success: function(data) {
				$('#searchResults').html(data);
				$('#searchResults').show();
				$('#content').hide();
			}
		});
	}
	$('.modal').click(function(e) {
		e.preventDefault();
		var url = $(this).attr('href');
		if (url.indexOf('#') == 0) {
			$(url).modal('open');
		} else {
			$.get(url, function(data) {
				$('<div class="modal hide fade">' + data + '</div>').modal();
			}).success(function() { $('input:text:visible:first').focus(); });
		}
	});
	$(document).tooltip({
		selector: '.tip'
	})
});
/**
 * Created by Bart on 8-12-13.
 */
$(document).ready(function() {
	$(document).on('mousedown', '.search-result', function() {
		$(this).toggleClass('active');
		if ($(this).hasClass('active')) {
			$(this).find('input[type="checkbox"]').prop('checked', true);
		} else {
			$(this).find('.tag').removeClass('active');
			$(this).find('radio').prop('selected', false);
			$(this).find('input[type="checkbox"]').prop('checked', false);
			$(this).find('input[type="radio"]').prop('checked', false);
		}
	})
	$(document).on('mousedown', '.tag', function(event) {
		$('.tag').removeClass('active');
		$(this).addClass('active');
		$(this).parents('.search-result').find('input[type=\'radio\']').prop('checked', true);

		if($(this).parents('.search-result').hasClass('active')) {
			event.stopPropagation();
		}
	})

});
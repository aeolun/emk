function searchItem() {
	window.location = root+'item/itemprice/'+$('#itemsearch').val().replace(/ /g,'_');
}
function addSS() {
	window.location = root+'item/addoverviewss/'+$('#solarsystem').val().replace(/ /g,'_');
}
$(document).ready(function() {
	$( ".itemsearch" ).autocomplete({
		source: root+'item/itempricehint',
		minLength: 2,
		delay:500
	});
	$( ".solarsystem" ).autocomplete({
		source: root+'item/solarsystemhint',
		minLength: 2,
		delay:500
	});
	$( ".regionsearch" ).autocomplete({
		source: root+'item/regionhint',
		minLength: 2,
		delay:500
	});
	$("#itemsearch").keydown(function(event) {
		if (event.keyCode==13) searchItem();
	});
	$("#solarsystem").keydown(function(event) {
		if (event.keyCode==13) addSS();
	});
	$( ".tabs" ).tabs({ 
		cookie: { expires: 7 },
		spinner: false,
		load: function (e, ui) {
			$(ui.panel).find(".tab-loading").remove();
		},
		show: function (e, ui) {
			var $panel = $(ui.panel);

			if ($panel.is(":empty")) {
				$panel.append("<div class='tab-loading'><img src=\""+root+'res/img/ajax-loader.gif'+"\" /> Loading...</div>")
			}
		}
	});
	/*if ($.cookie('menuopen') != null) {
		$('.submenu').css('display', $.cookie('menuopen'));
		if ($('.submenu').css('display') == 'block') $('#open').html('-');
	}*/
	var cookieName = 'stickyAccordion';
	$('.accordion').accordion({
		active:  parseInt($.cookie( cookieName )),
		change: function( event, ui )
		{
			$.cookie( cookieName, $( this ).find( 'h3' ).index ( ui.newHeader.get(0)));
		}
	});
	$(".images").msDropDown({visibleRows:5, rowHeight:16});
});
function addCommas(nStr)
{
	nStr = parseFloat(nStr);
	nStr = Math.round(nStr*100)/100;
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '.00';
	var rgx = /(\d+)(\d\d\d)/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}
/*
$(document).ready(function() {
		function searchSkill() {
			window.location = root+'corp/skillsearch?q='+$('#skillsearch').val();
		}
		function searchItem() {
			window.location = root+'corp/itemsearch?q='+$('#itemsearch').val();
		}
		$( "#skillsearch" ).autocomplete({
			source: root+'corp/skillsearchhint',
			minLength: 2,
			select: function( event, ui ) {
				searchSkill();
			}
		});
		$( "#itemsearch" ).autocomplete({
			source: root+'corp/itemsearchhint',
			minLength: 2,
			select: function( event, ui ) {
				searchItem();
			}
		});
	});
 */
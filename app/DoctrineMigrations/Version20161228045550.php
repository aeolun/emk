<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161228045550 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE banner (id INT AUTO_INCREMENT NOT NULL, image VARCHAR(1000) DEFAULT NULL, image_alt VARCHAR(1000) DEFAULT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE news (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(1000) DEFAULT NULL, posted_at DATE DEFAULT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("DROP TABLE orders_previous");
        $this->addSql("DROP TABLE orders_temp");
        $this->addSql("DROP TABLE transaction");
        $this->addSql("ALTER TABLE item_type_materials DROP FOREIGN KEY FK_3E3CF05174D6573C");
        $this->addSql("ALTER TABLE item_type_materials DROP FOREIGN KEY FK_3E3CF051C54C8C93");
        $this->addSql("DROP INDEX IDX_3E3CF051C54C8C93 ON item_type_materials");
        $this->addSql("DROP INDEX IDX_3E3CF05174D6573C ON item_type_materials");
        $this->addSql("ALTER TABLE item_type_material_cost DROP FOREIGN KEY FK_68F029BA98260155");
        $this->addSql("ALTER TABLE item_type_material_cost DROP FOREIGN KEY FK_68F029BAC54C8C93");
        $this->addSql("DROP INDEX IDX_68F029BAC54C8C93 ON item_type_material_cost");
        $this->addSql("DROP INDEX IDX_68F029BA98260155 ON item_type_material_cost");
        $this->addSql("ALTER TABLE item_type_material_cost CHANGE base_mineral_cost base_mineral_cost NUMERIC(10, 0) NOT NULL, CHANGE base_component_cost base_component_cost NUMERIC(10, 0) NOT NULL, CHANGE base_mineral_volume base_mineral_volume NUMERIC(10, 0) NOT NULL, CHANGE base_component_volume base_component_volume NUMERIC(10, 0) NOT NULL");
        $this->addSql("ALTER TABLE item_blueprint_types DROP FOREIGN KEY FK_757A54AB14959723");
        $this->addSql("ALTER TABLE item_blueprint_types DROP FOREIGN KEY FK_757A54AB22AB3297");
        $this->addSql("DROP INDEX UNIQ_757A54AB14959723 ON item_blueprint_types");
        $this->addSql("ALTER TABLE history CHANGE date date DATETIME NOT NULL");
        $this->addSql("ALTER TABLE industrial_type_requirements DROP FOREIGN KEY FK_A3EE8A57C54C8C93");
        $this->addSql("ALTER TABLE industrial_type_requirements DROP FOREIGN KEY FK_A3EE8A57E4ADAB53");
        $this->addSql("DROP INDEX IDX_A3EE8A57C54C8C93 ON industrial_type_requirements");
        $this->addSql("DROP INDEX IDX_A3EE8A57E4ADAB53 ON industrial_type_requirements");
        $this->addSql("ALTER TABLE orders DROP updated");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE orders_previous (id BIGINT NOT NULL, station_id INT DEFAULT NULL, region_id INT DEFAULT NULL, type_id INT DEFAULT NULL, solar_system_id INT NOT NULL, price NUMERIC(20, 2) NOT NULL, volume_remaining INT NOT NULL, volume_entered INT NOT NULL, volume_minimum INT NOT NULL, is_bid INT NOT NULL, issued DATETIME NOT NULL, duration INT NOT NULL, bullshit INT NOT NULL, updated DATETIME NOT NULL, INDEX new_index_1 (type_id, region_id), INDEX IDX_E52FFDEEC54C8C93 (type_id), INDEX IDX_E52FFDEE98260155 (region_id), INDEX IDX_E52FFDEE21BDB235 (station_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE orders_temp (id BIGINT NOT NULL, station_id INT DEFAULT NULL, region_id INT DEFAULT NULL, type_id INT DEFAULT NULL, solar_system_id INT NOT NULL, price NUMERIC(20, 2) NOT NULL, volume_remaining INT NOT NULL, volume_entered INT NOT NULL, volume_minimum INT NOT NULL, is_bid INT NOT NULL, issued DATETIME NOT NULL, duration INT NOT NULL, bullshit INT NOT NULL, updated DATETIME NOT NULL, INDEX IDX_A5864461C54C8C93 (type_id), INDEX IDX_A586446198260155 (region_id), INDEX IDX_A586446121BDB235 (station_id), INDEX IX_type_region (type_id, region_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE transaction (date DATE NOT NULL, type_id INT NOT NULL, station_id INT NOT NULL, sold INT NOT NULL, bought INT NOT NULL, INDEX IDX_723705D1C54C8C93 (type_id), INDEX IDX_723705D121BDB235 (station_id), INDEX IX_type_region (type_id, station_id), PRIMARY KEY(date, type_id, station_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE orders_previous ADD CONSTRAINT orders_previous_ibfk_1 FOREIGN KEY (station_id) REFERENCES map_denormalize (item_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE orders_previous ADD CONSTRAINT orders_previous_ibfk_2 FOREIGN KEY (region_id) REFERENCES map_regions (region_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE orders_previous ADD CONSTRAINT orders_previous_ibfk_3 FOREIGN KEY (type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE orders_temp ADD CONSTRAINT FK_A586446121BDB235 FOREIGN KEY (station_id) REFERENCES map_denormalize (item_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE orders_temp ADD CONSTRAINT FK_A586446198260155 FOREIGN KEY (region_id) REFERENCES map_regions (region_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE orders_temp ADD CONSTRAINT FK_A5864461C54C8C93 FOREIGN KEY (type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE transaction ADD CONSTRAINT FK_723705D121BDB235 FOREIGN KEY (station_id) REFERENCES map_denormalize (item_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE transaction ADD CONSTRAINT FK_723705D1C54C8C93 FOREIGN KEY (type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("DROP TABLE banner");
        $this->addSql("DROP TABLE news");
        $this->addSql("ALTER TABLE history CHANGE date date DATETIME NOT NULL");
        $this->addSql("ALTER TABLE industrial_type_requirements ADD CONSTRAINT FK_A3EE8A57C54C8C93 FOREIGN KEY (type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE industrial_type_requirements ADD CONSTRAINT FK_A3EE8A57E4ADAB53 FOREIGN KEY (required_type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_A3EE8A57C54C8C93 ON industrial_type_requirements (type_id)");
        $this->addSql("CREATE INDEX IDX_A3EE8A57E4ADAB53 ON industrial_type_requirements (required_type_id)");
        $this->addSql("ALTER TABLE item_blueprint_types ADD CONSTRAINT FK_757A54AB14959723 FOREIGN KEY (product_type_id) REFERENCES item_types (type_id)");
        $this->addSql("ALTER TABLE item_blueprint_types ADD CONSTRAINT FK_757A54AB22AB3297 FOREIGN KEY (blueprint_type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_757A54AB14959723 ON item_blueprint_types (product_type_id)");
        $this->addSql("ALTER TABLE item_type_material_cost CHANGE base_mineral_cost base_mineral_cost NUMERIC(10, 0) DEFAULT NULL, CHANGE base_component_cost base_component_cost NUMERIC(10, 0) DEFAULT NULL, CHANGE base_mineral_volume base_mineral_volume NUMERIC(10, 0) DEFAULT NULL, CHANGE base_component_volume base_component_volume NUMERIC(10, 0) DEFAULT NULL");
        $this->addSql("ALTER TABLE item_type_material_cost ADD CONSTRAINT FK_68F029BA98260155 FOREIGN KEY (region_id) REFERENCES map_regions (region_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE item_type_material_cost ADD CONSTRAINT FK_68F029BAC54C8C93 FOREIGN KEY (type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_68F029BAC54C8C93 ON item_type_material_cost (type_id)");
        $this->addSql("CREATE INDEX IDX_68F029BA98260155 ON item_type_material_cost (region_id)");
        $this->addSql("ALTER TABLE item_type_materials ADD CONSTRAINT FK_3E3CF05174D6573C FOREIGN KEY (material_type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE item_type_materials ADD CONSTRAINT FK_3E3CF051C54C8C93 FOREIGN KEY (type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_3E3CF051C54C8C93 ON item_type_materials (type_id)");
        $this->addSql("CREATE INDEX IDX_3E3CF05174D6573C ON item_type_materials (material_type_id)");
        $this->addSql("ALTER TABLE orders ADD updated DATETIME NOT NULL");
    }
}

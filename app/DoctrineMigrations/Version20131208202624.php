<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20131208202624 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE user ADD primary_region_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE user ADD CONSTRAINT FK_8D93D64952BDBCB2 FOREIGN KEY (primary_region_id) REFERENCES map_regions (region_id) ON DELETE SET NULL");
        $this->addSql("CREATE INDEX IDX_8D93D64952BDBCB2 ON user (primary_region_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE user DROP FOREIGN KEY FK_8D93D64952BDBCB2");
        $this->addSql("DROP INDEX IDX_8D93D64952BDBCB2 ON user");
        $this->addSql("ALTER TABLE user DROP primary_region_id");
    }
}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20131214132643 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE map_solar_system_jumps ADD CONSTRAINT FK_2065CCF7641482B2 FOREIGN KEY (to_solar_system_id) REFERENCES map_solar_systems (solar_system_id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_2065CCF7641482B2 ON map_solar_system_jumps (to_solar_system_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE map_solar_system_jumps DROP FOREIGN KEY FK_2065CCF7641482B2");
        $this->addSql("DROP INDEX IDX_2065CCF7641482B2 ON map_solar_system_jumps");
    }
}

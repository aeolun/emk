<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20131208174759 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");

        $this->addSql("CREATE TABLE map_region_user (user_id INT NOT NULL, map_region_id INT NOT NULL, INDEX IDX_B398933CA76ED395 (user_id), INDEX IDX_B398933C3D4E4608 (map_region_id), PRIMARY KEY(user_id, map_region_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("ALTER TABLE map_region_user ADD CONSTRAINT FK_B398933CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)");
        $this->addSql("ALTER TABLE map_region_user ADD CONSTRAINT FK_B398933C3D4E4608 FOREIGN KEY (map_region_id) REFERENCES map_regions (region_id)");
        $this->addSql("DROP TABLE history");
        $this->addSql("DROP TABLE laravel_migrations");
        $this->addSql("ALTER TABLE statistical_info CHANGE buy_min buy_min NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_max buy_max NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_average buy_average NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_mean buy_mean NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_percentile90 buy_percentile90 NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_percentile10 buy_percentile10 NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_min sell_min NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_max sell_max NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_average sell_average NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_mean sell_mean NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_percentile90 sell_percentile90 NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_percentile10 sell_percentile10 NUMERIC(10, 2) DEFAULT NULL, CHANGE updated updated DATETIME NOT NULL");
		$this->addSql("delete si from statistical_info si left join item_types it ON it.type_id = si.type_id WHERE it.type_id IS NULL");
        $this->addSql("ALTER TABLE statistical_info ADD CONSTRAINT FK_7CEE6233C54C8C93 FOREIGN KEY (type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE statistical_info ADD CONSTRAINT FK_7CEE623398260155 FOREIGN KEY (region_id) REFERENCES map_regions (region_id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_7CEE6233C54C8C93 ON statistical_info (type_id)");
        $this->addSql("CREATE INDEX IDX_7CEE623398260155 ON statistical_info (region_id)");
        $this->addSql("ALTER TABLE orders CHANGE type_id type_id INT DEFAULT NULL");
	    $this->addSql("delete si from orders si left join item_types it ON it.type_id = si.type_id WHERE it.type_id IS NULL");
        $this->addSql("ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEC54C8C93 FOREIGN KEY (type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_E52FFDEEC54C8C93 ON orders (type_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");

        $this->addSql("CREATE TABLE history (type_id INT NOT NULL, region_id INT NOT NULL, date DATETIME NOT NULL, low NUMERIC(20, 2) NOT NULL, high NUMERIC(20, 2) NOT NULL, average NUMERIC(20, 2) NOT NULL, orders INT NOT NULL, quantity BIGINT NOT NULL, PRIMARY KEY(type_id, region_id, date)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE laravel_migrations (bundle VARCHAR(50) NOT NULL, name VARCHAR(200) NOT NULL, batch INT NOT NULL, PRIMARY KEY(bundle, name)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("DROP TABLE map_region_user");
        $this->addSql("ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEEC54C8C93");
        $this->addSql("DROP INDEX IDX_E52FFDEEC54C8C93 ON orders");
        $this->addSql("ALTER TABLE orders CHANGE type_id type_id INT NOT NULL");
        $this->addSql("ALTER TABLE statistical_info DROP FOREIGN KEY FK_7CEE6233C54C8C93");
        $this->addSql("ALTER TABLE statistical_info DROP FOREIGN KEY FK_7CEE623398260155");
        $this->addSql("DROP INDEX IDX_7CEE6233C54C8C93 ON statistical_info");
        $this->addSql("DROP INDEX IDX_7CEE623398260155 ON statistical_info");
        $this->addSql("ALTER TABLE statistical_info CHANGE buy_min buy_min NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_max buy_max NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_average buy_average NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_mean buy_mean NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_percentile90 buy_percentile90 NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_percentile10 buy_percentile10 NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_min sell_min NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_max sell_max NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_average sell_average NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_mean sell_mean NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_percentile90 sell_percentile90 NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_percentile10 sell_percentile10 NUMERIC(20, 2) DEFAULT NULL, CHANGE updated updated DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL");
    }
}

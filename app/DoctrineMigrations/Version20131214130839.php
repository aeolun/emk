<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20131214130839 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE map_solar_system_distance (solar_system_id INT NOT NULL, to_solar_system_id INT DEFAULT NULL, distance INT DEFAULT NULL, is_safe TINYINT(1) DEFAULT NULL, PRIMARY KEY(solar_system_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("DROP TABLE Nodes");
        $this->addSql("ALTER TABLE history DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE history CHANGE low low NUMERIC(20, 2) DEFAULT NULL, CHANGE high high NUMERIC(20, 2) DEFAULT NULL, CHANGE average average NUMERIC(20, 2) DEFAULT NULL, CHANGE orders orders INT DEFAULT NULL, CHANGE quantity quantity BIGINT DEFAULT NULL");
        $this->addSql("ALTER TABLE history ADD PRIMARY KEY (date, region_id, type_id)");
        $this->addSql("ALTER TABLE map_solar_system_jumps ADD CONSTRAINT FK_2065CCF7ACCFA3CC FOREIGN KEY (from_solar_system_id) REFERENCES map_solar_systems (solar_system_id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_2065CCF7ACCFA3CC ON map_solar_system_jumps (from_solar_system_id)");
        $this->addSql("ALTER TABLE orders CHANGE price price NUMERIC(20, 2) NOT NULL");
        $this->addSql("ALTER TABLE statistical_info CHANGE buy_min buy_min NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_max buy_max NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_average buy_average NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_mean buy_mean NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_percentile90 buy_percentile90 NUMERIC(20, 2) DEFAULT NULL, CHANGE buy_percentile10 buy_percentile10 NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_min sell_min NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_max sell_max NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_average sell_average NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_mean sell_mean NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_percentile90 sell_percentile90 NUMERIC(20, 2) DEFAULT NULL, CHANGE sell_percentile10 sell_percentile10 NUMERIC(20, 2) DEFAULT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("CREATE TABLE Nodes (Id INT NOT NULL, Estimate NUMERIC(10, 3) NOT NULL, Predecessor INT DEFAULT NULL, Done TINYINT(1) NOT NULL, PRIMARY KEY(Id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("DROP TABLE map_solar_system_distance");
        $this->addSql("ALTER TABLE history DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE history CHANGE low low NUMERIC(20, 2) NOT NULL, CHANGE high high NUMERIC(20, 2) NOT NULL, CHANGE average average NUMERIC(20, 2) NOT NULL, CHANGE orders orders INT NOT NULL, CHANGE quantity quantity BIGINT NOT NULL");
        $this->addSql("ALTER TABLE history ADD PRIMARY KEY (type_id, region_id, date)");
        $this->addSql("ALTER TABLE map_solar_system_jumps DROP FOREIGN KEY FK_2065CCF7ACCFA3CC");
        $this->addSql("DROP INDEX IDX_2065CCF7ACCFA3CC ON map_solar_system_jumps");
        $this->addSql("ALTER TABLE orders CHANGE price price NUMERIC(10, 0) NOT NULL");
        $this->addSql("ALTER TABLE statistical_info CHANGE buy_min buy_min NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_max buy_max NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_average buy_average NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_mean buy_mean NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_percentile90 buy_percentile90 NUMERIC(10, 2) DEFAULT NULL, CHANGE buy_percentile10 buy_percentile10 NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_min sell_min NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_max sell_max NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_average sell_average NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_mean sell_mean NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_percentile90 sell_percentile90 NUMERIC(10, 2) DEFAULT NULL, CHANGE sell_percentile10 sell_percentile10 NUMERIC(10, 2) DEFAULT NULL");
    }
}

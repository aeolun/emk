<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20131214133443 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE map_solar_system_distance DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE map_solar_system_distance CHANGE to_solar_system_id to_solar_system_id INT NOT NULL, CHANGE is_safe is_safe TINYINT(1) NOT NULL");
        $this->addSql("ALTER TABLE map_solar_system_distance ADD PRIMARY KEY (solar_system_id, to_solar_system_id, is_safe)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE map_solar_system_distance DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE map_solar_system_distance CHANGE to_solar_system_id to_solar_system_id INT DEFAULT NULL, CHANGE is_safe is_safe TINYINT(1) DEFAULT NULL");
        $this->addSql("ALTER TABLE map_solar_system_distance ADD PRIMARY KEY (solar_system_id)");
    }
}

<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20131214145856 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE history ADD CONSTRAINT FK_27BA704B98260155 FOREIGN KEY (region_id) REFERENCES map_regions (region_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE history ADD CONSTRAINT FK_27BA704BC54C8C93 FOREIGN KEY (type_id) REFERENCES item_types (type_id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_27BA704B98260155 ON history (region_id)");
        $this->addSql("CREATE INDEX IDX_27BA704BC54C8C93 ON history (type_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE history DROP FOREIGN KEY FK_27BA704B98260155");
        $this->addSql("ALTER TABLE history DROP FOREIGN KEY FK_27BA704BC54C8C93");
        $this->addSql("DROP INDEX IDX_27BA704B98260155 ON history");
        $this->addSql("DROP INDEX IDX_27BA704BC54C8C93 ON history");
    }
}

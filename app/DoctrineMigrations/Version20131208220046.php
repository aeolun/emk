<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20131208220046 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE orders CHANGE station_id station_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE21BDB235 FOREIGN KEY (station_id) REFERENCES map_denormalize (item_id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_E52FFDEE21BDB235 ON orders (station_id)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE21BDB235");
        $this->addSql("DROP INDEX IDX_E52FFDEE21BDB235 ON orders");
        $this->addSql("ALTER TABLE orders CHANGE station_id station_id INT NOT NULL");
    }
}
